package pl.crystalofchocolate.telegram.repository;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.response.SendResponse;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.crystalofchocolate.config.AppProperties;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TelegramRepositoryTest {

  @Test
  @DisplayName("TelegramRepository test")
  void telegramRepositoryTest() {

    AppProperties appProperties = AppProperties.builder()
        .telegramToken("fake3432token")
        .build();
    TelegramConnection telegramConnection = TelegramConnection.of(appProperties);

    TelegramBot mockTelegramBot = mock(TelegramBot.class);
    SendResponse sendResponseSuccessfully = mock(SendResponse.class);
    SendResponse sendResponseFail = mock(SendResponse.class);
    TelegramRepository telegramRepository = new TelegramRepository(telegramConnection, mockTelegramBot);

    when(sendResponseSuccessfully.isOk()).thenReturn(true);
    when(sendResponseSuccessfully.errorCode()).thenReturn(200);
    when(sendResponseSuccessfully.description()).thenReturn("This is mock success description");
    when(sendResponseFail.isOk()).thenReturn(false);
    when(sendResponseFail.errorCode()).thenReturn(404);
    when(sendResponseFail.description()).thenReturn("This is mock fail description");

    when(mockTelegramBot.execute(any(SendMessage.class))).thenAnswer(invocationOnMock -> {
      SendMessage sendMessage = invocationOnMock.getArgument(0);
      long chatId = (long) sendMessage.getParameters().get("chat_id");
      String message = (String) sendMessage.getParameters().get("text");
      return chatId > 0 && Objects.nonNull(message) && message.length() > 0 ? sendResponseSuccessfully : sendResponseFail;
    });

    assertFalse(telegramRepository.isSendToDefaultChannelEnabled());
    assertNull(telegramRepository.getDefaultChannelId());
    assertEquals(telegramConnection.getDefaultChanelId(), telegramRepository.getDefaultChannelId());

    assertTrue(telegramRepository.sendMessage(new SendMessage(10L, "Test message")));
    assertFalse(telegramRepository.sendMessage(new SendMessage(0L, "Test message")));
    assertFalse(telegramRepository.sendMessage(new SendMessage(10L, "")));

    verify(mockTelegramBot, times(3)).execute(any());
    verify(sendResponseFail, times(4)).isOk();
    verify(sendResponseSuccessfully, times(2)).isOk();

  }

}