package pl.crystalofchocolate.telegram.filter;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pl.crystalofchocolate.config.AppProperties;

import java.util.List;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

class TelegramFilterBuilderTest {

  @Test
  @DisplayName("Telegram filter builder test")
  void buildTelegramFilters() {

    TelegramFilterBuilder telegramFilterBuilder = new TelegramFilterBuilder();
    AppProperties emptyConfig = AppProperties.builder()
        .configProperties(new Properties())
        .build();

    assertDoesNotThrow(() -> telegramFilterBuilder.buildTelegramFilters(emptyConfig));
    assertEquals(0, telegramFilterBuilder.buildTelegramFilters(emptyConfig).size());

    Properties properties = buildNonFilterProperties();
    AppProperties nonFilterConfig = AppProperties.builder()
        .configProperties(properties)
        .build();

    List<TelegramFilter> telegramFilters = telegramFilterBuilder.buildTelegramFilters(nonFilterConfig);
    assertEquals(0, telegramFilters.size());

    properties = buildFilterProperties();
    AppProperties filterConfig = AppProperties.builder()
        .configProperties(properties)
        .build();

    telegramFilters = telegramFilterBuilder.buildTelegramFilters(filterConfig);
    assertEquals(3, telegramFilters.size());
    assertNotSame(telegramFilters.get(0), telegramFilters.get(1));
    assertNotSame(telegramFilters.get(1), telegramFilters.get(2));

    assertEquals("filter1", telegramFilters.get(0).getFilterName());
    assertEquals("fileName1", telegramFilters.get(0).getFilesNames().get(0));
    assertEquals("test subject form test", telegramFilters.get(0).getSubjects().get(0));
    assertEquals("test1@test.com", telegramFilters.get(0).getAuthors().get(0));
    assertEquals("pdf", telegramFilters.get(0).getFilesExtensions().get(0));
    assertEquals("here message sent when match file", telegramFilters.get(0).getMessage());
    assertEquals(3, telegramFilters.get(0).getChatsIds().size());
    assertTrue(telegramFilters.get(0).getChatsIds().contains(1));
    assertTrue(telegramFilters.get(0).getChatsIds().contains(2));
    assertTrue(telegramFilters.get(0).getChatsIds().contains(3));

    assertEquals("filter3", telegramFilters.get(2).getFilterName());
    assertNull(telegramFilters.get(2).getFilesNames());
    assertEquals("test subject3 form test", telegramFilters.get(2).getSubjects().get(0));
    assertNull(telegramFilters.get(2).getAuthors());
    assertNull(telegramFilters.get(2).getChatsIds());
    assertNull(telegramFilters.get(2).getMessage());
  }

  private Properties buildFilterProperties() {
    Properties properties = new Properties();

    properties.setProperty("filter.telegram.filter1.file.name", "fileName1");
    properties.setProperty("filter.telegram.filter1.subject", "test subject form test");
    properties.setProperty("filter.telegram.filter1.author", "test1@test.com");
    properties.setProperty("filter.telegram.filter1.file.extension", "pdf");
    properties.setProperty("filter.telegram.filter1.message", "here message sent when match file");
    properties.setProperty("filter.telegram.filter1.channels", "1,2,3");

    properties.setProperty("filter.telegram.filter2.file.name", "fileName2");
    properties.setProperty("filter.telegram.filter2.subject", "test subject2 form test");
    properties.setProperty("filter.telegram.filter2.author", "test2@test.com");

    properties.setProperty("filter.telegram.filter3.subject", "test subject3 form test");

    return properties;
  }

  private Properties buildNonFilterProperties() {
    Properties properties = new Properties();

    properties.setProperty("connection.ftp.address", "127.0.0.1");
    properties.setProperty("connection.ftp.user", "user");
    properties.setProperty("connection.ftp.password", "password");
    properties.setProperty("connection.ftp.dictionary", "/test/dictionary");
    properties.setProperty("connection.ftp.store.in.directory.if.filter.no.match", "false");

    properties.setProperty("connection.mail.host", "imap.mailbox");
    properties.setProperty("connection.mail.port", "993");
    properties.setProperty("connection.mail.user", "user");
    properties.setProperty("connection.mail.password", "password");

    properties.setProperty("filter.ftp.filter3.subject", "test subject3 form test");

    return properties;
  }
}