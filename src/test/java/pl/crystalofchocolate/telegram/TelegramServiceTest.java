package pl.crystalofchocolate.telegram;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.crystalofchocolate.picker.attachment.CollectedAttachment;
import pl.crystalofchocolate.telegram.filter.TelegramFilter;
import pl.crystalofchocolate.telegram.repository.TelegramRepository;

import java.time.LocalDateTime;
import java.util.*;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class TelegramServiceTest {

  @Test
  @DisplayName("Test collect attachment by Telegram Service")
  void collect_attachment_non_match_test_when_default_channel_not_enabled() {

    List<TelegramFilter> telegramFilters = prepareFilters();
    TelegramRepository mockTelegramRepository = mock(TelegramRepository.class);
    TelegramService telegramService = new TelegramService(mockTelegramRepository, telegramFilters);

    when(mockTelegramRepository.isSendToDefaultChannelEnabled()).thenReturn(false);

    CollectedAttachment collectedAttachment = CollectedAttachment.builder()
        .name("file-name.txt")
        .content("example context".getBytes())
        .receivedDate(LocalDateTime.now())
        .build();

    assertDoesNotThrow(() -> telegramService.collectAttachment(collectedAttachment));

    verify(mockTelegramRepository, times(1)).isSendToDefaultChannelEnabled();
    verify(mockTelegramRepository, never()).sendMessage(any());
    verify(mockTelegramRepository, never()).getDefaultChannelId();
  }

  @Test
  @DisplayName("Test collect attachment by Telegram Service")
  void collect_attachment_non_match_test_when_default_channel_enabled() {

    List<TelegramFilter> telegramFilters = prepareFilters();
    TelegramRepository mockTelegramRepository = mock(TelegramRepository.class);
    TelegramService telegramService = new TelegramService(mockTelegramRepository, telegramFilters);

    when(mockTelegramRepository.isSendToDefaultChannelEnabled()).thenReturn(true);
    when(mockTelegramRepository.getDefaultChannelId()).thenReturn(10);

    CollectedAttachment collectedAttachment = CollectedAttachment.builder()
        .name("file-name.txt")
        .content("example context".getBytes())
        .receivedDate(LocalDateTime.now())
        .build();

    assertDoesNotThrow(() -> telegramService.collectAttachment(collectedAttachment));

    verify(mockTelegramRepository, times(1)).isSendToDefaultChannelEnabled();
    verify(mockTelegramRepository, times(1)).sendMessage(any());
    verify(mockTelegramRepository, times(1)).getDefaultChannelId();
  }

  @Test
  @DisplayName("Test collect attachment by Telegram Service")
  void collect_attachment_match_test() {

    TelegramFilter telegramFilter = new TelegramFilter("MockTestFilter");
    telegramFilter.setChatsIds(List.of(10, 20));
    telegramFilter.setFilesNames(List.of("mock-file"));
    telegramFilter.setFilesExtensions(List.of("mp3"));

    TelegramFilter telegramFilter2 = new TelegramFilter("MockTestFilter2");
    telegramFilter2.setChatsIds(Arrays.asList(10, 20));
    telegramFilter2.setMessage("Mock Message");
    telegramFilter2.setFilesNames(List.of("mock-file"));
    telegramFilter2.setFilesExtensions(List.of("mp3"));

    TelegramRepository mockTelegramRepository = mock(TelegramRepository.class);
    TelegramService telegramService = new TelegramService(mockTelegramRepository, List.of(telegramFilter, telegramFilter2));

    when(mockTelegramRepository.sendMessage(any())).thenReturn(true);

    CollectedAttachment collectedAttachment = CollectedAttachment.builder()
        .name("mock-file.mp3")
        .content("example context".getBytes())
        .receivedDate(LocalDateTime.now())
        .build();

    assertDoesNotThrow(() -> telegramService.collectAttachment(collectedAttachment));

    verify(mockTelegramRepository, never()).isSendToDefaultChannelEnabled();
    verify(mockTelegramRepository, times(6)).sendMessage(any());
    verify(mockTelegramRepository, never()).getDefaultChannelId();
  }

  private List<TelegramFilter> prepareFilters() {
    List<TelegramFilter> telegramFilters = new ArrayList<>();

    TelegramFilter telegramFilter = new TelegramFilter("MockTestFilter");
    telegramFilter.setChatsIds(Arrays.asList(10, 20));
    telegramFilter.setMessage("Mock Message");
    telegramFilter.setFilesNames(List.of("mock-file"));
    telegramFilter.setFilesExtensions(List.of("mp3"));

    telegramFilters.add(telegramFilter);
    return telegramFilters;
  }

}