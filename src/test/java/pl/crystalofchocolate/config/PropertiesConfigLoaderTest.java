package pl.crystalofchocolate.config;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class PropertiesConfigLoaderTest {

  @Test
  @DisplayName("Validate PropertiesConfigLoader")
  void loadConfigFromFile() throws Exception {
    String filePart = "src/test/resources/example-config.properties";
    ConfigLoader configLoader = new PropertiesConfigLoader();
    AppProperties config = configLoader.loadConfigFromFile(filePart);
    assertNotNull(config);

    assertEquals("Inbox", config.getAppMailFolderName());
    assertEquals("10", config.getAppTaskPoolSize());
    assertEquals("20", config.getAppFtpCheckTime());
    assertEquals("MINUTES", config.getAppFtpCheckTimeUnit());
    assertEquals("2", config.getAppMailCheckTime());
    assertEquals("SECONDS", config.getAppMailCheckTimeUnit());

    assertEquals("ftp.host.ip", config.getFtpAddress());
    assertEquals("ftp_user", config.getFtpUser());
    assertEquals("ftp_pass", config.getFtpPassword());
    assertEquals("212", config.getFtpPort());
    assertEquals("ftp/Documents", config.getFtpDictionary());
    assertEquals("true", config.getFtpStoreInMainDirectoryIfFilterNotMatch());

    assertEquals("imap.host.mail", config.getMailHost());
    assertEquals("993", config.getMailPort());
    assertEquals("user@mail", config.getMailUser());
    assertEquals("pass_user", config.getMailPassword());
    assertEquals("imap", config.getMailProtocol());
    assertEquals("true", config.getMailSSL());

    assertEquals("10", config.getTelegramDefaultChannelId());
    assertEquals("token here", config.getTelegramToken());
    assertEquals("path", config.getStorePath());

  }
}