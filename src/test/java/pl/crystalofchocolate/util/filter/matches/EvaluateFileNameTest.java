package pl.crystalofchocolate.util.filter.matches;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class EvaluateFileNameTest {

  @Test
  @DisplayName("Evaluate file name")
  void evaluateFileName() {
    String attachmentName = "attachment-name.pdf";
    List<String> testFileNamePatterns = Collections.emptyList();

    assertTrue(EvaluateFileName.evaluateFilesNames(null, attachmentName));
    assertFalse(EvaluateFileName.evaluateFilesNames(testFileNamePatterns, attachmentName));

    testFileNamePatterns = List.of("text-1", "text-2", "text-3");
    assertFalse(EvaluateFileName.evaluateFilesNames(testFileNamePatterns, attachmentName));

    testFileNamePatterns = List.of("text-1", "text-2", "attachment-name");
    assertTrue(EvaluateFileName.evaluateFilesNames(testFileNamePatterns, attachmentName));

    testFileNamePatterns = List.of("text-1", "attachment-name", "text-2", "attachment-name", "text-3");
    assertTrue(EvaluateFileName.evaluateFilesNames(testFileNamePatterns, attachmentName));
  }
}