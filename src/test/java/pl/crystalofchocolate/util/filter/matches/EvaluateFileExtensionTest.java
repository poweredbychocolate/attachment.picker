package pl.crystalofchocolate.util.filter.matches;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class EvaluateFileExtensionTest {

  @Test
  @DisplayName("Evaluate file extension")
  void evaluateFileExtension() {
    String attachmentName = "attachment-name.pdf";
    List<String> testFileExtensionPatterns = Collections.emptyList();

    assertTrue(EvaluateFileExtension.evaluateFilesExtensions(null, attachmentName));
    assertFalse(EvaluateFileExtension.evaluateFilesExtensions(testFileExtensionPatterns, attachmentName));

    testFileExtensionPatterns = List.of("txt", "json", "sql");
    assertFalse(EvaluateFileExtension.evaluateFilesExtensions(testFileExtensionPatterns, attachmentName));

    testFileExtensionPatterns = List.of("txt", "json", "sql", "pdf");
    assertTrue(EvaluateFileExtension.evaluateFilesExtensions(testFileExtensionPatterns, attachmentName));

    testFileExtensionPatterns = List.of("txt", "json", "pdf", "sql", "pdf");
    assertTrue(EvaluateFileExtension.evaluateFilesExtensions(testFileExtensionPatterns, attachmentName));
  }
}