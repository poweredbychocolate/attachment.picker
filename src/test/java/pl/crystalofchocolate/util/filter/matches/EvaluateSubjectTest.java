package pl.crystalofchocolate.util.filter.matches;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class EvaluateSubjectTest {

  @Test
  @DisplayName("Evaluate author test")
  void evaluateSubject() {
    String attachmentSubject = "subject-5";
    List<String> testSubjectsPatterns = Collections.emptyList();

    assertTrue(EvaluateSubject.evaluateSubjects(null, attachmentSubject));
    assertFalse(EvaluateSubject.evaluateSubjects(testSubjectsPatterns, attachmentSubject));

    testSubjectsPatterns = List.of("subject-1", "subject-2", "subject-3");
    assertFalse(EvaluateSubject.evaluateSubjects(testSubjectsPatterns, attachmentSubject));

    testSubjectsPatterns = List.of("subject-1", "subject-2", "subject-5");
    assertTrue(EvaluateSubject.evaluateSubjects(testSubjectsPatterns, attachmentSubject));

    testSubjectsPatterns = List.of("subject-1", "subject-5", "subject-3", "subject");
    assertTrue(EvaluateSubject.evaluateSubjects(testSubjectsPatterns, attachmentSubject));
  }
}