package pl.crystalofchocolate.util.filter.matches;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class EvaluateAuthorTest {

  @Test
  @DisplayName("Evaluate author test")
  void evaluateAuthors() {
    String testAddress = "author5@test.java";
    List<String> testAddressPatterns = Collections.emptyList();

    assertTrue(EvaluateAuthor.evaluateAuthors(null, testAddress));
    assertFalse(EvaluateAuthor.evaluateAuthors(testAddressPatterns, testAddress));

    testAddressPatterns = List.of("author1@test.java", "author2@test.java", "author3@test.java");
    assertFalse(EvaluateAuthor.evaluateAuthors(testAddressPatterns, testAddress));

    testAddressPatterns = List.of("author1@test.java", "author2@test.java", "author5@test.java");
    assertTrue(EvaluateAuthor.evaluateAuthors(testAddressPatterns, testAddress));

    testAddressPatterns = List.of(
        "author1@test.java",
        "author2@test.java",
        "author5@test.java",
        "author4@test.java",
        "author5@test.java"
    );
    assertTrue(EvaluateAuthor.evaluateAuthors(testAddressPatterns, testAddress));
  }
}