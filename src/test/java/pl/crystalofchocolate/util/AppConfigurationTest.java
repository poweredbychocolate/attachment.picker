package pl.crystalofchocolate.util;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pl.crystalofchocolate.config.AppProperties;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class AppConfigurationTest {

  @Test
  @DisplayName("test when app properties is empty")
  void test_with_empty_configuration() {
    AppProperties appProperties = AppProperties.builder().build();
    AppConfiguration appConfiguration = AppConfiguration.of(appProperties);

    assertEquals("Inbox", appConfiguration.getAppMailFolderName());
    assertEquals(4, appConfiguration.getAppTaskPoolSize());
    assertEquals(10L, appConfiguration.getAppFtpCheckTime());
    assertEquals(TimeUnit.MINUTES, appConfiguration.getAppFtpCheckTimeUnit());
    assertEquals(1L, appConfiguration.getAppMailCheckTime());
    assertEquals(TimeUnit.MINUTES, appConfiguration.getAppMailCheckTimeUnit());
  }

  @Test
  @DisplayName("test when app properties is given")
  void test_with_configuration() {
    AppProperties appProperties = AppProperties.builder()
        .appMailFolderName("Fantastic Mail Box")
        .appTaskPoolSize("36")
        .appFtpCheckTime("40")
        .appFtpCheckTimeUnit(TimeUnit.SECONDS.name())
        .appMailCheckTime("3")
        .appMailCheckTimeUnit(TimeUnit.HOURS.name())
        .build();
    AppConfiguration appConfiguration = AppConfiguration.of(appProperties);

    assertEquals("Fantastic Mail Box", appConfiguration.getAppMailFolderName());
    assertEquals(36, appConfiguration.getAppTaskPoolSize());
    assertEquals(40L, appConfiguration.getAppFtpCheckTime());
    assertEquals(TimeUnit.SECONDS, appConfiguration.getAppFtpCheckTimeUnit());
    assertEquals(3L, appConfiguration.getAppMailCheckTime());
    assertEquals(TimeUnit.HOURS, appConfiguration.getAppMailCheckTimeUnit());
  }
}