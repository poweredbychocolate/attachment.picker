package pl.crystalofchocolate.util;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pl.crystalofchocolate.picker.attachment.CollectedAttachment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class NameUtilTest {

  @Test
  @DisplayName("Validate of prepare file name with date prefix from collected attachment")
  void prepareFileName() throws ParseException {
    String stringDate = "2021-02-20 13:45:23";
    String stringDatePattern = "yyyy-MM-dd HH:mm:ss";

    SimpleDateFormat dateFormat = new SimpleDateFormat(stringDatePattern);
    Date parsedDate = dateFormat.parse(stringDate);

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(stringDatePattern);
    LocalDateTime parsedLocalDateTime = LocalDateTime.parse(stringDate, formatter);

    CollectedAttachment collectedAttachment = CollectedAttachment.builder()
        .receivedDate(parsedLocalDateTime)
        .name("Prepared-file-name.pdf")
        .content(parsedDate.toString().getBytes())
        .build();

    String parsedName = NameUtil.prepareFileNameWithDate(collectedAttachment);
    assertEquals("2021_02_20-13_45_23-Prepared-file-name.pdf", parsedName);
  }


  @Test
  @DisplayName("Test date to string conversion and back")
  void dateConversion() throws ParseException {
    String stringDate = "2021-01-23 08:34:59";
    String stringDatePattern = "yyyy-MM-dd HH:mm:ss";

    SimpleDateFormat dateFormat = new SimpleDateFormat(stringDatePattern);
    Date parsedDate = dateFormat.parse(stringDate);

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(stringDatePattern);
    LocalDateTime parsedLocalDateTime = LocalDateTime.parse(stringDate, formatter);

    String localDateTimeToString = NameUtil.localDateTimeToString(parsedLocalDateTime);
    assertEquals("2021_01_23-08_34_59-", localDateTimeToString);

    LocalDateTime localDateTimeFromString = NameUtil.stringToLocalDateTime(localDateTimeToString);
    assertEquals(parsedLocalDateTime, localDateTimeFromString);

    var tmpLocalDateTime = LocalDateTime.of(1994, 1, 15, 14, 15, 36);
    var tmpDateString = NameUtil.localDateTimeToString(tmpLocalDateTime);
    assertEquals("1994_01_15-14_15_36-", tmpDateString);

    var tmpDateFromString = NameUtil.stringToLocalDateTime("1994_01_15-14_15_36-");
    assertEquals(tmpLocalDateTime, tmpDateFromString);

    assertEquals(parsedLocalDateTime, NameUtil.dateToLocalDateTime(parsedDate));
  }
}