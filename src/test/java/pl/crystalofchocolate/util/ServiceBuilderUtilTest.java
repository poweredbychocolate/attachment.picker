package pl.crystalofchocolate.util;

import org.junit.jupiter.api.*;
import pl.crystalofchocolate.config.AppProperties;
import pl.crystalofchocolate.ftp.FTPDestinationService;
import pl.crystalofchocolate.ftp.FTPService;
import pl.crystalofchocolate.mail.MailService;
import pl.crystalofchocolate.storage.StoreService;
import pl.crystalofchocolate.telegram.TelegramService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

class ServiceBuilderUtilTest {

  @BeforeAll
  @AfterAll
  static void cleanTmpDirectory() {
    Path path = Path.of("src/test/resources/tmp");
    if (Files.exists(path)) {
      removeFile(path);
    }
  }

  private static void removeFile(Path path) {
    try {
      if (Files.isDirectory(path) && Files.list(path).findFirst().isPresent()) {
        Files.list(path).forEach(ServiceBuilderUtilTest::removeFile);
      }
      Files.delete(path);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Test
  @DisplayName("Build telegram service")
  @Order(0)
  void buildTelegramService() {
    AppProperties appProperties = AppProperties.builder()
        .telegramToken("MockTokenHere:e323")
        .telegramDefaultChannelId("12345")
        .build();

    assertDoesNotThrow(() -> {
      TelegramService telegramService = ServiceBuilderUtil.buildTelegramService(appProperties);
      assertNotNull(telegramService);
    });
  }

  @Test
  @DisplayName("Build ftp service")
  @Order(0)
  void buildFtpService() {

    AppProperties appProperties = AppProperties.builder()
        .ftpPort("21")
        .ftpAddress("localhost")
        .ftpDictionary("mainDirectory")
        .ftpStoreInMainDirectoryIfFilterNotMatch(Boolean.FALSE.toString())
        .ftpUser("user")
        .ftpPassword("pass")
        .build();

    assertDoesNotThrow(() -> {
      FTPService ftpService = ServiceBuilderUtil.buildFtpService(appProperties);
      assertNotNull(ftpService);
    });
  }

  @Test
  @DisplayName("Build store service")
  @Order(0)
  void buildStoreService() throws IOException {
    Path storePath = Path.of("src/test/resources/tmp");
    if (!Files.exists(storePath)) {
      Files.createDirectory(storePath);
    }
    AppProperties appProperties = AppProperties.builder()
        .storePath(storePath.toString())
        .build();

    assertDoesNotThrow(() -> {
      StoreService storeService = ServiceBuilderUtil.buildStoreService(appProperties);
      assertNotNull(storeService);
    });
  }

  @Test
  @DisplayName("Build ftp destination service")
  @Order(1)
  void buildFtpDestinationService() throws IOException {
    Path storePath = Path.of("src/test/resources/tmp");
    if (!Files.exists(storePath)) {
      Files.createDirectory(storePath);
    }
    AppProperties appProperties = AppProperties.builder()
        .storePath(storePath.toString())
        .ftpPort("21")
        .ftpAddress("localhost")
        .ftpDictionary("mainDirectory")
        .ftpStoreInMainDirectoryIfFilterNotMatch(Boolean.FALSE.toString())
        .ftpUser("user")
        .ftpPassword("pass")
        .build();

    final StoreService storeService = ServiceBuilderUtil.buildStoreService(appProperties);
    final FTPService ftpService = ServiceBuilderUtil.buildFtpService(appProperties);

    assertDoesNotThrow(() -> {
      FTPDestinationService ftpDestinationService = ServiceBuilderUtil.buildFtpDestinationService(ftpService, storeService, appProperties);
      assertNotNull(ftpDestinationService);
    });
  }

  @Test
  @DisplayName("Build mail service")
  @Order(0)
  void buildMailService() {
    AppProperties appProperties = AppProperties.builder()
        .mailHost("mail@host.com")
        .mailPort("993")
        .mailUser("user")
        .mailPassword("pass")
        .build();

    assertDoesNotThrow(() -> {
      MailService mailService = ServiceBuilderUtil.buildMailService(appProperties);
      assertNotNull(mailService);
    });
  }
}