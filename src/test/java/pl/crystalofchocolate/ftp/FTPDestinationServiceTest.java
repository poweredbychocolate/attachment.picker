package pl.crystalofchocolate.ftp;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.crystalofchocolate.ftp.filter.FTPFilter;
import pl.crystalofchocolate.picker.attachment.CollectedAttachment;
import pl.crystalofchocolate.storage.StoreService;

import java.io.IOException;
import java.net.NoRouteToHostException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class FTPDestinationServiceTest {
  @Mock
  private FTPService ftpService;
  @Mock
  private StoreService storeService;

  @Test
  @DisplayName("Test collect attachment without filters")
  void test_collect_attachment_without_filters() throws IOException {
    FTPDestinationService destinationService = new FTPDestinationService(ftpService, storeService, Collections.emptyList());
    CollectedAttachment testAttachment = CollectedAttachment.builder()
        .name("test.txt")
        .content("test attachment context" .getBytes())
        .receivedDate(LocalDateTime.now())
        .build();

    assertDoesNotThrow(() -> destinationService.collectAttachment(testAttachment));
    assertDoesNotThrow(() -> destinationService.collectAttachment(testAttachment));
    assertDoesNotThrow(() -> destinationService.collectAttachment(testAttachment));

    verify(ftpService, never()).storeAttachment(anyString(), any());
    verify(storeService, never()).storeAttachment(any(), anyString(), any());
  }

  @Test
  @DisplayName("Test collect attachment with filters")
  void test_collect_attachment_with_filters() throws IOException {
    FTPFilter matchFtpFilter = new FTPFilter("Ftp filter for mock test");
    matchFtpFilter.setFilesNames(List.of("test"));
    matchFtpFilter.setFilesExtensions(List.of("txt"));
    matchFtpFilter.setStoreSubdirectory("filter/files");

    FTPFilter nonMatchFtpFilter = new FTPFilter("Ftp filter for mock test");
    nonMatchFtpFilter.setFilesNames(List.of("test"));
    nonMatchFtpFilter.setFilesExtensions(List.of("pdf"));
    nonMatchFtpFilter.setStoreSubdirectory("filter/files/pdf");

    FTPDestinationService destinationService = new FTPDestinationService(ftpService, storeService, List.of(matchFtpFilter, nonMatchFtpFilter));
    CollectedAttachment testAttachment = CollectedAttachment.builder()
        .name("test.txt")
        .content("test attachment context" .getBytes())
        .receivedDate(LocalDateTime.now())
        .build();

    doNothing().when(storeService).storeAttachment(any(), anyString(), any());
    doNothing().when(ftpService).storeAttachment(anyString(), any());

    assertDoesNotThrow(() -> destinationService.collectAttachment(testAttachment));

    doThrow(NoRouteToHostException.class).when(ftpService).storeAttachment(anyString(), any());
    assertDoesNotThrow(() -> destinationService.collectAttachment(testAttachment));

    doThrow(IOException.class).when(ftpService).storeAttachment(anyString(), any());
    doThrow(IOException.class).when(storeService).storeAttachment(any(), anyString(), any());
    assertDoesNotThrow(() -> destinationService.collectAttachment(testAttachment));

    verify(ftpService, times(3)).storeAttachment(anyString(), any());
    verify(ftpService, times(1)).close();
    verify(storeService, times(2)).storeAttachment(any(), anyString(), any());
  }
}