package pl.crystalofchocolate.ftp;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.crystalofchocolate.storage.StoreService;
import pl.crystalofchocolate.util.NameUtil;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class FTPLocalFileLoaderServiceTest {
  @Mock
  private FTPService ftpService;
  @Mock
  private StoreService storeService;

  @Test
  @DisplayName(" FTPLocalFileLoaderService test")
  void test() throws IOException {
    FTPLocalFileLoaderService ftpLocalFileLoaderService = FTPLocalFileLoaderService.builder()
        .ftpService(ftpService)
        .storeService(storeService)
        .delayTimeValue(2L)
        .delayTimeUnit(TimeUnit.MINUTES)
        .build();
    Properties properties = new Properties();
    properties.setProperty(NameUtil.FTP_DATE, NameUtil.localDateTimeToString(LocalDateTime.now()));

    when(ftpService.isOnline()).thenReturn(true);
    when(storeService.getFilesList(anyString())).thenReturn(List.of("file1.txt", "file2.txt"));
    when(storeService.fetchAttachmentContent(anyString(), anyString())).thenReturn("mock file context".getBytes());
    when(storeService.fetchAttachmentProperties(anyString(), anyString())).thenReturn(properties);
    doNothing().when(ftpService).storeAttachment(anyString(), any());
    when(storeService.removeAttachment(anyString(), anyString())).thenReturn(true);

    assertEquals(2L, ftpLocalFileLoaderService.getDelayTimeValue());
    assertEquals(TimeUnit.MINUTES, ftpLocalFileLoaderService.getDelayTimeUnit());

    when(storeService.removeAttachment(anyString(), anyString())).thenThrow(IOException.class);
    properties.setProperty(NameUtil.FTP_SUBDIRECTORY, "mock/store");
    assertDoesNotThrow(ftpLocalFileLoaderService::run);

    verify(ftpService, times(1)).isOnline();
    verify(storeService, times(1)).getFilesList(anyString());
    verify(storeService, times(2)).fetchAttachmentContent(anyString(), anyString());
    verify(storeService, times(2)).fetchAttachmentProperties(anyString(), anyString());
    verify(ftpService, times(2)).storeAttachment(anyString(), any());
    verify(storeService, times(2)).removeAttachment(anyString(), anyString());
  }

}