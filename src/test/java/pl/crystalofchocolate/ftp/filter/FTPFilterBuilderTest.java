package pl.crystalofchocolate.ftp.filter;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pl.crystalofchocolate.config.AppProperties;

import java.util.List;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

class FTPFilterBuilderTest {

  @Test
  @DisplayName("Ftp filter builder test")
  void buildFtpFilters() {

    FTPFilterBuilder ftpFilterBuilder = new FTPFilterBuilder();
    AppProperties emptyConfig = AppProperties.builder()
        .configProperties(new Properties())
        .build();

    assertDoesNotThrow(() -> ftpFilterBuilder.buildFtpFilters(emptyConfig));
    assertEquals(0, ftpFilterBuilder.buildFtpFilters(emptyConfig).size());

    Properties properties = buildNonFilterProperties();
    AppProperties nonFilterConfig = AppProperties.builder()
        .configProperties(properties)
        .build();

    List<FTPFilter> ftpFilters = ftpFilterBuilder.buildFtpFilters(nonFilterConfig);
    assertEquals(0, ftpFilters.size());

    properties = buildFilterProperties();
    AppProperties filterConfig = AppProperties.builder()
        .configProperties(properties)
        .build();

    ftpFilters = ftpFilterBuilder.buildFtpFilters(filterConfig);
    assertEquals(3, ftpFilters.size());
    assertNotSame(ftpFilters.get(0), ftpFilters.get(1));
    assertNotSame(ftpFilters.get(1), ftpFilters.get(2));

    assertEquals("filter1", ftpFilters.get(0).getFilterName());
    assertEquals("fileName1.test", ftpFilters.get(0).getFilesNames().get(0));
    assertEquals("test subject form test", ftpFilters.get(0).getSubjects().get(0));
    assertEquals("test1@test.com", ftpFilters.get(0).getAuthors().get(0));
    assertEquals("/documents/to/pay", ftpFilters.get(0).getStoreSubdirectory());

    assertEquals("filter3", ftpFilters.get(2).getFilterName());
    assertNull(ftpFilters.get(2).getFilesNames());
    assertEquals("test subject3 form test", ftpFilters.get(2).getSubjects().get(0));
    assertNull(ftpFilters.get(2).getAuthors());
    assertNull(ftpFilters.get(2).getStoreSubdirectory());
  }

  private Properties buildFilterProperties() {
    Properties properties = new Properties();

    properties.setProperty("filter.ftp.filter1.file.name", "fileName1.test");
    properties.setProperty("filter.ftp.filter1.subject", "test subject form test");
    properties.setProperty("filter.ftp.filter1.author", "test1@test.com");
    properties.setProperty("filter.ftp.filter1.store.directory", "/documents/to/pay");

    properties.setProperty("filter.ftp.filter2.file.name", "fileName2.test");
    properties.setProperty("filter.ftp.filter2.subject", "test subject2 form test");
    properties.setProperty("filter.ftp.filter2.author", "test2@test.com");
    properties.setProperty("filter.ftp.filter2.store.directory", "/documents/old/2020");

    properties.setProperty("filter.ftp.filter3.subject", "test subject3 form test");

    return properties;
  }

  private Properties buildNonFilterProperties() {
    Properties properties = new Properties();

    properties.setProperty("connection.ftp.address", "127.0.0.1");
    properties.setProperty("connection.ftp.user", "user");
    properties.setProperty("connection.ftp.password", "password");
    properties.setProperty("connection.ftp.dictionary", "/test/dictionary");
    properties.setProperty("connection.ftp.store.in.directory.if.filter.no.match", "false");

    properties.setProperty("connection.mail.host", "imap.mailbox");
    properties.setProperty("connection.mail.port", "993");
    properties.setProperty("connection.mail.user", "user");
    properties.setProperty("connection.mail.password", "password");

    properties.setProperty("filter.telegram.filter3.subject", "test subject3 form test");

    return properties;
  }
}