package pl.crystalofchocolate.ftp.filter;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pl.crystalofchocolate.picker.attachment.CollectedAttachment;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FTPFilterTest {

  @Test
  @DisplayName("Ftp filter test when non match")
  void noneMatch() {
    CollectedAttachment collectedAttachment = CollectedAttachment.builder()
        .name("file1.txt")
        .content("content1".getBytes())
        .withSubject("No match subject")
        .fromAddress("unknown@address.com")
        .build();

    FTPFilter ftpFilter = new FTPFilter("Test Filter");
    ftpFilter.setFilesNames(List.of("test-file"));
    assertFalse(ftpFilter.match(collectedAttachment));

    ftpFilter.setFilesNames(null);
    ftpFilter.setFilesExtensions(List.of("pdf"));
    assertFalse(ftpFilter.match(collectedAttachment));

    ftpFilter.setFilesExtensions(null);
    ftpFilter.setSubjects(List.of("Test subject"));
    assertFalse(ftpFilter.match(collectedAttachment));

    ftpFilter.setSubjects(null);
    ftpFilter.setAuthors(List.of("test.author@filter.com"));
    assertFalse(ftpFilter.match(collectedAttachment));

    ftpFilter.setFilesNames(List.of("test-file"));
    ftpFilter.setFilesExtensions(List.of("pdf"));
    ftpFilter.setSubjects(List.of("Test subject"));
    ftpFilter.setAuthors(List.of("test.author@filter.com"));
    assertFalse(ftpFilter.match(collectedAttachment));
  }

  @Test
  @DisplayName("Ftp filter test when match by single field only")
  void singleMatch() {
    CollectedAttachment collectedAttachment = CollectedAttachment.builder()
        .name("file1.txt")
        .content("content1".getBytes())
        .withSubject("Single match subject")
        .fromAddress("test.author@filter.com")
        .build();

    FTPFilter ftpFilter = new FTPFilter("Test Filter");
    ftpFilter.setFilesNames(List.of("file1"));
    assertTrue(ftpFilter.match(collectedAttachment));

    ftpFilter.setFilesNames(null);
    ftpFilter.setFilesExtensions(List.of("txt"));
    assertTrue(ftpFilter.match(collectedAttachment));

    ftpFilter.setFilesExtensions(null);
    ftpFilter.setSubjects(List.of("match subject"));
    assertTrue(ftpFilter.match(collectedAttachment));

    ftpFilter.setSubjects(null);
    ftpFilter.setAuthors(List.of("test.author@filter.com"));
    assertTrue(ftpFilter.match(collectedAttachment));
  }

  @Test
  @DisplayName("Ftp filter test when match by few fields")
  void multiMatch() {
    CollectedAttachment collectedAttachment = CollectedAttachment.builder()
        .name("file1.txt")
        .content("content1".getBytes())
        .withSubject("Single match subject")
        .fromAddress("test.author@filter.com")
        .build();

    FTPFilter ftpFilter = new FTPFilter("Test Filter");
    ftpFilter.setFilesNames(List.of("file1"));
    ftpFilter.setFilesExtensions(List.of("txt"));
    assertTrue(ftpFilter.match(collectedAttachment));

    ftpFilter.setFilesNames(null);
    ftpFilter.setFilesExtensions(null);
    ftpFilter.setSubjects(List.of("match subject"));
    ftpFilter.setAuthors(List.of("test.author@filter.com"));
    assertTrue(ftpFilter.match(collectedAttachment));

    ftpFilter.setFilesNames(List.of("file1"));
    ftpFilter.setFilesExtensions(List.of("mp3"));
    ftpFilter.setSubjects(List.of("match subject"));
    ftpFilter.setAuthors(List.of("test.author@filter.com"));
    assertFalse(ftpFilter.match(collectedAttachment));

    ftpFilter.setFilesNames(null);
    ftpFilter.setFilesExtensions(null);
    ftpFilter.setSubjects(null);
    ftpFilter.setFilesExtensions(null);
    assertTrue(ftpFilter.match(collectedAttachment));
  }
}