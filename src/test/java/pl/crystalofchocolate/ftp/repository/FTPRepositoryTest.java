package pl.crystalofchocolate.ftp.repository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockftpserver.fake.FakeFtpServer;
import org.mockftpserver.fake.UserAccount;
import org.mockftpserver.fake.filesystem.DirectoryEntry;
import org.mockftpserver.fake.filesystem.FileEntry;
import org.mockftpserver.fake.filesystem.UnixFakeFileSystem;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class FTPRepositoryTest {

  @Test
  @DisplayName("Test ftp repository when server is online")
  void test_when_server_is_online() {
    FakeFtpServer fakeFtpServer = prepareServer();
    fakeFtpServer.setServerControlPort(21);
    fakeFtpServer.start();

    FTPConnection ftpConnection = FTPConnection.builder()
        .address("127.0.0.1")
        .dictionary("/data")
        .user("user")
        .password("password")
        .build();

    FTPRepository ftpRepository = new FTPRepository(ftpConnection);
    assertTrue(ftpRepository.isOnline());
    assertTrue(ftpRepository.isFileExist("test.txt"));

    assertDoesNotThrow(() -> ftpRepository.switchToSubdirectory("/sub"));
    assertFalse(ftpRepository.isFileExist("test.txt"));
    assertTrue(ftpRepository.isFileExist("subtest.txt"));

    assertDoesNotThrow(() -> ftpRepository.storeFile("store1.txt", new ByteArrayInputStream("store 1 context".getBytes())));
    assertTrue(ftpRepository.isFileExist("store1.txt"));

    ftpRepository.close();
    fakeFtpServer.stop();
  }

  @Test
  @DisplayName("Test ftp repository when server is offline")
  void test_when_server_is_not_available() {
    FakeFtpServer fakeFtpServer = prepareServer();
    fakeFtpServer.setServerControlPort(34);
    fakeFtpServer.start();

    FTPConnection ftpConnection = FTPConnection.builder()
        .address("127.0.0.1")
        .dictionary("/data")
        .user("user")
        .password("password")
        .port(234)
        .build();

    FTPRepository ftpRepository = new FTPRepository(ftpConnection);
    assertFalse(ftpRepository.isOnline());
    assertFalse(ftpRepository.isFileExist("test.txt"));

    assertThrows(IOException.class, () -> ftpRepository.switchToSubdirectory("/sub"));
    assertFalse(ftpRepository.isFileExist("test.txt"));
    assertFalse(ftpRepository.isFileExist("subtest.txt"));

    assertFalse(ftpRepository.isFileExist("test.txt"));
    assertFalse(ftpRepository.isFileExist("subtest.txt"));

    assertThrows(IOException.class, () -> ftpRepository.storeFile("store1.txt", new ByteArrayInputStream("store 1 context".getBytes())));
    assertFalse(ftpRepository.isFileExist("store1.txt"));

    ftpRepository.close();
    fakeFtpServer.stop();
  }

  private FakeFtpServer prepareServer() {
    FakeFtpServer fakeFtpServer = new FakeFtpServer();
    fakeFtpServer.addUserAccount(new UserAccount("user", "password", "/data"));

    UnixFakeFileSystem fileSystem = new UnixFakeFileSystem();
    fileSystem.add(new DirectoryEntry("/data"));
    fileSystem.add(new FileEntry("/data/test.txt", "File Entry"));

    fileSystem.add(new DirectoryEntry("/data/sub"));
    fileSystem.add(new FileEntry("/data/sub/subtest.txt", "File Entry"));

    fakeFtpServer.setFileSystem(fileSystem);
    return fakeFtpServer;
  }
}