package pl.crystalofchocolate.ftp;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.crystalofchocolate.ftp.repository.FTPRepository;
import pl.crystalofchocolate.picker.attachment.CollectedAttachment;

import java.io.IOException;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class FTPServiceTest {

  @Mock
  private FTPRepository ftpRepository;

  @Test
  @DisplayName("FTP service test store in subdirectory")
  void store_in_subdirectory_test() throws IOException {

    FTPService ftpService = new FTPService(ftpRepository);
    CollectedAttachment testAttachment = CollectedAttachment.builder()
        .name("test-attachment.txt")
        .content("test attachment context".getBytes())
        .receivedDate(LocalDateTime.now())
        .build();
    String subdirectory = "subdirectory";

    doNothing().when(ftpRepository).storeFile(anyString(), any());
    doNothing().when(ftpRepository).switchToSubdirectory(anyString());
    doNothing().when(ftpRepository).close();
    when(ftpRepository.isFileExist(anyString())).thenReturn(false);
    when(ftpRepository.isOnline()).thenReturn(true);

    assertDoesNotThrow(() -> ftpService.storeAttachment(subdirectory, testAttachment));

    when(ftpRepository.isFileExist(anyString())).thenReturn(true);

    assertDoesNotThrow(() -> ftpService.storeAttachment(subdirectory, testAttachment));
    assertDoesNotThrow(ftpService::close);
    assertDoesNotThrow(() -> assertTrue(ftpService.isOnline()));

    verify(ftpRepository, times(2)).storeFile(anyString(), any());
    verify(ftpRepository, times(2)).isFileExist(anyString());
    verify(ftpRepository, times(2)).switchToSubdirectory(anyString());

  }

}