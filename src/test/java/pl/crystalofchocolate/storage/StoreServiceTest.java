package pl.crystalofchocolate.storage;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pl.crystalofchocolate.picker.attachment.CollectedAttachment;
import pl.crystalofchocolate.storage.repository.StoreConnection;
import pl.crystalofchocolate.storage.repository.StoreRepository;
import pl.crystalofchocolate.util.NameUtil;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

class StoreServiceTest {

  @BeforeAll
  @AfterAll
  static void cleanTmpDirectory() {
    Path path = Path.of("src/test/resources/tmp");
    if (Files.exists(path)) {
      removeFile(path);
    }
  }

  private static void removeFile(Path path) {
    try {
      if (Files.isDirectory(path) && Files.list(path).findFirst().isPresent()) {
        Files.list(path).forEach(StoreServiceTest::removeFile);
      }
      Files.delete(path);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Test
  @DisplayName("Store service test")
  void storeAndRemoveAttachment() {
    StoreService storeService = new StoreService(new StoreRepository(StoreConnection.builder().path("src/test/resources/tmp").build()));

    CollectedAttachment collectedAttachment = CollectedAttachment.builder()
        .content("test context1".getBytes())
        .name("text1.txt")
        .receivedDate(LocalDateTime.now())
        .build();

    String directoryPath = "test3";

    assertDoesNotThrow(() -> storeService.storeAttachment(collectedAttachment, directoryPath, null));
    assertDoesNotThrow(() -> storeService.storeAttachment(collectedAttachment, directoryPath, null));
    assertThrows(IOException.class, () -> storeService.storeAttachment(collectedAttachment, directoryPath, null));

    assertDoesNotThrow(() -> storeService.removeAttachment(collectedAttachment.getName(), directoryPath));
    assertDoesNotThrow(() -> storeService.removeAttachment(collectedAttachment.getName(), directoryPath));
    assertDoesNotThrow(() -> storeService.removeAttachment(NameUtil.prepareFileNameWithDate(collectedAttachment), directoryPath));

    Properties properties = new Properties();
    properties.put("Key.1", "Value.1");
    properties.put("Key.2", "Value.2");
    properties.put("Key.3", "Value.3");
    properties.put("Key.4", "Value.4");
    assertDoesNotThrow(() -> storeService.storeAttachment(collectedAttachment, directoryPath, properties));

    assertDoesNotThrow(() -> {
      List<String> filesList = storeService.getFilesList(directoryPath);
      assertEquals(2, filesList.size());
      assertEquals("text1.txt", filesList.get(0));
      assertEquals("text1.txt.metadata", filesList.get(1));
    });

    assertDoesNotThrow(() -> {
      byte[] collectedAttachmentBytes = storeService.fetchAttachmentContent(directoryPath, "text1.txt");
      assertEquals(collectedAttachment.getContent().length, collectedAttachmentBytes.length);
      assertEquals(new String(collectedAttachment.getContent()), new String(collectedAttachmentBytes));
    });
    assertThrows(IOException.class, () -> storeService.fetchAttachmentContent(directoryPath, "file234.txt"));

    assertDoesNotThrow(() -> {
      Properties fileMetadataProperties = storeService.fetchAttachmentProperties(directoryPath, "text1.txt");
      assertEquals(4, fileMetadataProperties.size());
      assertEquals(properties, fileMetadataProperties);
    });
    assertDoesNotThrow(() -> storeService.removeAttachment(collectedAttachment.getName(), directoryPath));
  }
}