package pl.crystalofchocolate.storage.repository;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

class StoreRepositoryTest {

  @BeforeAll
  @AfterAll
  static void cleanTmpDirectory() {
    Path path = Path.of("src/test/resources/tmp");
    if (Files.exists(path)) {
      removeFile(path);
    }
  }

  private static void removeFile(Path path) {
    try {
      if (Files.isDirectory(path) && Files.list(path).findFirst().isPresent()) {
        Files.list(path).forEach(StoreRepositoryTest::removeFile);
      }
      Files.delete(path);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Test
  @DisplayName("StoreConnection test")
  void storeConnectionTest() {
    StoreConnection storeConnection = StoreConnection.builder()
        .path("src/test/resources/tmp")
        .build();
    assertEquals("src/test/resources/tmp", storeConnection.getPath());
    assertTrue(storeConnection.toString().contains("src/test/resources/tmp"));
  }

  @Test
  @DisplayName("Store Repository test")
  void storeRepositoryTest() throws IOException {
    StoreRepository storeRepository = new StoreRepository(StoreConnection.builder()
        .path("src/test/resources/tmp")
        .build());

    assertDoesNotThrow(() -> storeRepository.openDirectory("test1"));
    assertDoesNotThrow(() -> storeRepository.openDirectory("test2"));
    assertDoesNotThrow(() -> storeRepository.openDirectory("test1"));

    assertFalse(storeRepository.isFileExists("testFile1.txt"));
    assertDoesNotThrow(() -> storeRepository.saveFile("testFile1.txt", "test data 1".getBytes()));
    assertTrue(storeRepository.isFileExists("testFile1.txt"));
    assertTrue(storeRepository.filesList().contains("testFile1.txt"));

    assertTrue(storeRepository.removeFile("testFile1.txt"));
    assertFalse(storeRepository.removeFile("testFile1.txt"));

    assertDoesNotThrow(() -> storeRepository.openDirectory("test2"));

    assertFalse(storeRepository.isFileExists("testFile2.txt"));
    assertDoesNotThrow(() -> storeRepository.saveFile("testFile2.txt", "test data 2".getBytes()));
    assertEquals("test data 2".getBytes().length, storeRepository.fetchFileBytes("testFile2.txt").length);
    assertTrue(storeRepository.isFileExists("testFile2.txt"));
    assertTrue(storeRepository.removeFile("testFile2.txt"));
    assertFalse(storeRepository.removeFile("testFile2.txt"));

  }

}