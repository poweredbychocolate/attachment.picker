package pl.crystalofchocolate.picker.attachment;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CollectedAttachmentMapperTest {

  @Test
  @DisplayName("Test collected attachment mapper")
  void mapTest() throws IOException, MessagingException {
    MimeMessage message = mock(MimeMessage.class);
    MimeBodyPart bodyPart = mock(MimeBodyPart.class);
    InputStream mockInputStream = new ByteArrayInputStream("simple file content for test".getBytes());
    Address[] mockFrom = new Address[1];
    mockFrom[0] = new InternetAddress("runner@some.tea");

    when(bodyPart.getInputStream()).thenReturn(mockInputStream);
    when(bodyPart.getFileName()).thenReturn("tea.txt");
    when(message.getReceivedDate()).thenReturn(new Date());
    when(message.getSubject()).thenReturn("Can you buy some tea?");
    when(message.getFrom()).thenReturn(mockFrom);

    assertDoesNotThrow(() -> {
      CollectedAttachment collectedAttachment = CollectedAttachmentMapper.map(message, bodyPart);
      assertNotNull(collectedAttachment);
    });
  }

  @Test
  @DisplayName("decode file name from mime encoded name")
  void decodeFileNameTest() {
    String notEncodedText = "Simple text message with żźćńłśąę€ó";
    String quotedPrintableText = "=?utf-8?Q?Simple text message with" +
        " =C5=BC=C5=BA=C4=87=C5=84=C5=82=C5=9B=C4=85=C4=99=E2=82=AC=C3=B3?=";

    assertDoesNotThrow(() -> {
      assertEquals(notEncodedText, CollectedAttachmentMapper.decodeFileName(notEncodedText));
      assertEquals(notEncodedText, CollectedAttachmentMapper.decodeFileName(quotedPrintableText));
    });
  }
}