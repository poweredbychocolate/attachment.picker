package pl.crystalofchocolate.picker;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.crystalofchocolate.mail.MailService;
import pl.crystalofchocolate.picker.attachment.CollectedAttachmentDestination;
import pl.crystalofchocolate.util.sheduled.ScheduledUtil;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.event.MessageCountEvent;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class PickerServiceTest {

  @Test
  @DisplayName("PickerService test MessageCountListener#messagesAdded")
  void messagesAdded() throws IOException, MessagingException {
    CollectedAttachmentDestination mockDestination = mock(CollectedAttachmentDestination.class);
    List<CollectedAttachmentDestination> attachmentDestinations = List.of(mockDestination);
    MailService mailService = mock(MailService.class);
    ScheduledUtil scheduledUtil = mock(ScheduledUtil.class);
    PickerService pickerService = new PickerService(mailService, attachmentDestinations, scheduledUtil);

    MessageCountEvent mockMessageCountEvent = mock(MessageCountEvent.class);
    Message mockMessage = mock(MimeMessage.class);
    MimeBodyPart mockBodyPart1 = mock(MimeBodyPart.class);
    MimeBodyPart mockBodyPart2 = mock(MimeBodyPart.class);

    Message[] messages = new Message[1];
    messages[0] = mockMessage;
    Address[] mockAddresses = new Address[1];
    mockAddresses[0] = new InternetAddress("test@mock");

    when(mockMessageCountEvent.getMessages()).thenReturn(messages);
    when(mockMessage.getSubject()).thenReturn("Mock test 01");
    when(mockMessage.getReceivedDate()).thenReturn(new Date());
    when(mockMessage.getFrom()).thenReturn(mockAddresses);

    doAnswer(invocationOnMock -> {
      mockDestination.collectAttachment(any());
      return null;
    }).when(scheduledUtil).submit(any());
    doNothing().when(mockDestination).collectAttachment(any());

    when(mockBodyPart1.getFileName()).thenReturn("fileName1.txt");
    when(mockBodyPart1.getDisposition()).thenReturn(Part.ATTACHMENT);
    when(mockBodyPart1.getInputStream()).thenReturn(new ByteArrayInputStream("mockBodyPart1".getBytes()));
    when(mockBodyPart2.getFileName()).thenReturn("fileName2.txt");
    when(mockBodyPart2.getDisposition()).thenReturn(Part.ATTACHMENT);
    when(mockBodyPart2.getInputStream()).thenReturn(new ByteArrayInputStream("mockBodyPart2".getBytes()));

    when(mailService.extractAttachmentsFromMessage(any()))
        .thenReturn(List.of(mockBodyPart1, mockBodyPart2));
    assertDoesNotThrow(() -> pickerService.messagesAdded(mockMessageCountEvent));

    when(mockBodyPart2.getDisposition()).thenThrow(MessagingException.class);
    assertDoesNotThrow(() -> pickerService.messagesAdded(mockMessageCountEvent));

    when(mailService.extractAttachmentsFromMessage(any())).thenThrow(MessagingException.class);
    assertDoesNotThrow(() -> pickerService.messagesAdded(mockMessageCountEvent));

    verify(mockMessageCountEvent, times(3)).getMessages();
    verify(mailService, times(3)).extractAttachmentsFromMessage(any());

    verify(mockBodyPart1, times(4)).getDisposition();
    verify(mockBodyPart1, times(12)).getFileName();
    verify(mockBodyPart1, times(2)).getInputStream();

    verify(mockBodyPart2, times(3)).getDisposition();
    verify(mockBodyPart2, times(6)).getFileName();
    verify(mockBodyPart2, times(1)).getInputStream();

    verify(scheduledUtil, times(3)).submit(any());
    verify(mockDestination, times(3)).collectAttachment(any());
  }

  @Test
  @DisplayName("PickerService test if MessageCountListener#messagesRemoved does not thrown exception")
  void messagesRemoved() {
    MailService mailService = mock(MailService.class);
    ScheduledUtil scheduledUtil = mock(ScheduledUtil.class);
    PickerService pickerService = new PickerService(mailService, Collections.emptyList(), scheduledUtil);
    assertDoesNotThrow(() -> pickerService.messagesRemoved(mock(MessageCountEvent.class)));
  }
}