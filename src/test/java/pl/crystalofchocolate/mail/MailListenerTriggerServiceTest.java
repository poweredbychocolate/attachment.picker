package pl.crystalofchocolate.mail;

import com.sun.mail.imap.IMAPFolder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.mail.MessagingException;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MailListenerTriggerServiceTest {

  @Test
  @DisplayName("Mail listener trigger service runnable thread test")
  void triggerRunTest() throws MessagingException {
    IMAPFolder imapFolder = mock(IMAPFolder.class);
    MailListenerTriggerService mailListenerTriggerService = MailListenerTriggerService.builder()
        .imapInboxFolder(imapFolder)
        .delayTimeValue(2L)
        .delayTimeUnit(TimeUnit.MINUTES)
        .build();

    when(imapFolder.isOpen()).thenReturn(true);
    when(imapFolder.getMessageCount()).thenReturn(10);

    mailListenerTriggerService.run();

    when(imapFolder.getMessageCount()).thenThrow(MessagingException.class);

    assertDoesNotThrow(mailListenerTriggerService::run);
    verify(imapFolder, times(2)).isOpen();
    verify(imapFolder, times(2)).getMessageCount();
  }
}