package pl.crystalofchocolate.mail.repository;

import com.sun.mail.imap.IMAPFolder;
import org.junit.jupiter.api.Test;

import javax.mail.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class MailRepositoryTest {

  @Test
  void repositoryTest() throws MessagingException {

    MailServerConnection mailServerConnection = MailServerConnection.builder()
        .host("localhost")
        .port(993)
        .user("test")
        .password("test")
        .build();

    Session mockSession = mock(Session.class);
    Store mockStore = mock(Store.class);
    IMAPFolder mockImapFolder = mock(IMAPFolder.class);
    when(mockSession.getStore()).thenReturn(mockStore);
    doNothing().when(mockStore).connect(anyString(), anyString());
    when(mockStore.getFolder("inbox")).thenReturn(mockImapFolder);
    when(mockStore.getFolder("other")).thenThrow(MessagingException.class);

    assertDoesNotThrow(() -> {
      MailRepository mailRepository = new MailRepository(mailServerConnection, mockSession);
      assertNotNull(mailRepository);

      Folder folder = mailRepository.getMailStoreFolder("inbox");
      assertEquals(mockImapFolder, folder);
      assertThrows(MessagingException.class, () -> mailRepository.getMailStoreFolder("other"));
      mailRepository.close();
    });

    verify(mockSession, times(1)).getStore();
    verify(mockStore, times(2)).connect(anyString(), anyString());
    verify(mockStore, times(2)).getFolder(anyString());
    verify(mockStore, times(1)).close();
    verify(mockImapFolder, times(1)).close();
  }

}