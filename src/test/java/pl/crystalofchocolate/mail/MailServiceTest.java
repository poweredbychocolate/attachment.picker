package pl.crystalofchocolate.mail;

import com.sun.mail.imap.IMAPFolder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.crystalofchocolate.mail.repository.MailRepository;

import javax.mail.*;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MailServiceTest {

  @Test
  @DisplayName("Open mail folder test")
  void openMailStoreFolder() throws MessagingException {
    MailRepository mockMailRepository = mock(MailRepository.class);
    IMAPFolder imapFolder = mock(IMAPFolder.class);
    MailService mailService = new MailService(mockMailRepository);

    when(mockMailRepository.getMailStoreFolder(anyString())).thenReturn(imapFolder);
    when(imapFolder.isOpen()).thenReturn(true);

    assertDoesNotThrow(() -> {
      IMAPFolder folder = mailService.openMailStoreFolder("Inbox", Folder.READ_WRITE);
      assertNotNull(folder);
      assertTrue(folder.isOpen());
    });

    verify(mockMailRepository, times(1)).getMailStoreFolder(anyString());
    verify(imapFolder, times(1)).isOpen();
    verify(imapFolder, times(1)).open(anyInt());
  }

  @Test
  @DisplayName("Test extract attachments from message")
  void extractAttachmentsFromMessage() throws MessagingException, IOException {
    MailRepository mockMailRepository = mock(MailRepository.class);
    MimeMessage mockInputMessage = mock(MimeMessage.class);
    MimeMultipart mockMultipart = mock(MimeMultipart.class);

    MailService mailService = new MailService(mockMailRepository);

    BodyPart bodyPart1 = new MimeBodyPart();
    bodyPart1.setContent("content01", "content01");
    bodyPart1.setText("text01");

    BodyPart bodyPart2 = new MimeBodyPart();
    bodyPart2.setContent("content02", "content02");
    bodyPart2.setText("text02");

    when(mockInputMessage.getContent()).thenReturn(mockMultipart);
    when(mockMultipart.getCount()).thenReturn(2);
    when(mockMultipart.getBodyPart(anyInt()))
        .thenReturn(bodyPart1)
        .thenReturn(bodyPart2);

    assertDoesNotThrow(() -> {
      List<MimeBodyPart> extractList = mailService.extractAttachmentsFromMessage(mockInputMessage);
      assertEquals(2, extractList.size());
      assertTrue(extractList.contains(bodyPart1));
      assertTrue(extractList.contains(bodyPart2));
      assertNotSame(extractList.get(0), extractList.get(1));
    });

    verify(mockInputMessage, times(1)).getContent();
    verify(mockMultipart, times(4)).getCount();
    verify(mockMultipart, times(2)).getBodyPart(anyInt());
  }
}