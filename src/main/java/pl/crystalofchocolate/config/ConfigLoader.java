package pl.crystalofchocolate.config;

import java.io.IOException;

public interface ConfigLoader {
  AppProperties loadConfigFromFile(String configFilePath) throws IOException;
}
