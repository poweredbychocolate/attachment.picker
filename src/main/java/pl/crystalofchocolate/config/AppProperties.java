package pl.crystalofchocolate.config;

import lombok.*;

import java.util.Properties;

@Value
@Builder
public class AppProperties {
  String appMailFolderName;
  String appTaskPoolSize;
  String appFtpCheckTime;
  String appFtpCheckTimeUnit;
  String appMailCheckTimeUnit;
  String appMailCheckTime;

  String mailHost;
  String mailUser;
  String mailPassword;
  String mailPort;
  String mailSSL;
  String mailProtocol;

  String ftpAddress;
  String ftpPort;
  String ftpUser;
  String ftpPassword;
  String ftpDictionary;
  String ftpStoreInMainDirectoryIfFilterNotMatch;

  String telegramDefaultChannelId;
  String telegramToken;

  String storePath;
  Properties configProperties;
}
