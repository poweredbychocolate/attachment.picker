package pl.crystalofchocolate.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesConfigLoader implements ConfigLoader {

  @Override
  public AppProperties loadConfigFromFile(String configFilePath) throws IOException {
    Properties configProperties = new Properties();

    try(InputStream fileInputStream = new FileInputStream(configFilePath)){
      configProperties.load(fileInputStream);
    }

    String storeInDirectoryKey = "connection.ftp.store.in.directory.if.filter.no.match";
    String storeInDirectoryValue = configProperties.getProperty(storeInDirectoryKey, "false");

    return AppProperties.builder()
        .configProperties(configProperties)

        .appMailFolderName(configProperties.getProperty("app.mail.folder.name"))
        .appTaskPoolSize(configProperties.getProperty("app.tasks.pool.size"))
        .appMailCheckTime(configProperties.getProperty("app.check.time.mail"))
        .appMailCheckTimeUnit(configProperties.getProperty("app.check.time.unit.mail"))
        .appFtpCheckTime(configProperties.getProperty("app.check.time.ftp"))
        .appFtpCheckTimeUnit(configProperties.getProperty("app.check.time.unit.ftp"))

        .mailHost(configProperties.getProperty("connection.mail.host"))
        .mailPort(configProperties.getProperty("connection.mail.port"))
        .mailUser(configProperties.getProperty("connection.mail.user"))
        .mailPassword(configProperties.getProperty("connection.mail.password"))
        .mailSSL(configProperties.getProperty("connection.mail.ssl", null))
        .mailProtocol(configProperties.getProperty("connection.mail.protocol", null))

        .ftpAddress(configProperties.getProperty("connection.ftp.address"))
        .ftpUser(configProperties.getProperty("connection.ftp.user"))
        .ftpPassword(configProperties.getProperty("connection.ftp.password"))
        .ftpDictionary(configProperties.getProperty("connection.ftp.dictionary"))
        .ftpPort(configProperties.getProperty("connection.ftp.port"))
        .ftpStoreInMainDirectoryIfFilterNotMatch(storeInDirectoryValue)

        .telegramToken(configProperties.getProperty("connection.telegram.token"))
        .telegramDefaultChannelId(configProperties.getProperty("connection.telegram.default.channel.id"))

        .storePath(configProperties.getProperty("connection.store.path"))
        .build();
  }
}
