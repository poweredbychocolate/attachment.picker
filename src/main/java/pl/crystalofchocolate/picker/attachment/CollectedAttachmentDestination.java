package pl.crystalofchocolate.picker.attachment;

public interface CollectedAttachmentDestination {
  void collectAttachment(CollectedAttachment collectedAttachment);
}
