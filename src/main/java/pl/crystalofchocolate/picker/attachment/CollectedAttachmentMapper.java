package pl.crystalofchocolate.picker.attachment;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import javax.mail.internet.ParseException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import static pl.crystalofchocolate.util.NameUtil.dateToLocalDateTime;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public class CollectedAttachmentMapper {
  public static CollectedAttachment map(MimeMessage message, MimeBodyPart bodyPart) throws MessagingException, IOException, java.text.ParseException {
    log.debug(String.format("Map CollectedAttachment start: %s : %s", bodyPart.getFileName(), LocalDateTime.now()));
    log.debug(bodyPart.getFileName());
    log.debug(bodyPart.getContentType());
    log.debug(bodyPart.getDescription());
    log.debug(bodyPart.getDisposition());
    log.debug(bodyPart.getEncoding());

    byte[] contentBytes = bodyPartAsContentBytes(bodyPart);

    log.debug(String.format("Map CollectedAttachment end stream processing: %s : %s", bodyPart.getFileName(), LocalDateTime.now()));

    return CollectedAttachment.builder()
        .fromAddress(message.getFrom()[0].toString())
        .withSubject(message.getSubject())
        .receivedDate(dateToLocalDateTime(message.getReceivedDate()))
        .content(contentBytes)
        .name(decodeFileName(bodyPart.getFileName()))
        .build();
  }

  private static byte[] bodyPartAsContentBytes(MimeBodyPart bodyPart) throws MessagingException, IOException {
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    bodyPart.getInputStream().transferTo(byteArrayOutputStream);
    byteArrayOutputStream.flush();
    byte[] contentBytes = byteArrayOutputStream.toByteArray();
    byteArrayOutputStream.close();
    return contentBytes;
  }

  public static String decodeFileName(String inputFileName) throws UnsupportedEncodingException, ParseException {
    if (inputFileName.startsWith("=?")) {
      return MimeUtility.decodeWord(inputFileName);
    }
    return inputFileName;
  }
}
