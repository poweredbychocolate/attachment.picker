package pl.crystalofchocolate.picker.attachment;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.time.LocalDateTime;

@Builder
@Value
public class CollectedAttachment {
  @NonNull
  String name;
  @NonNull
  byte[] content;
  String fromAddress;
  String withSubject;
  LocalDateTime receivedDate;
}
