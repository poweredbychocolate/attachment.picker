package pl.crystalofchocolate.picker;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.crystalofchocolate.mail.MailService;
import pl.crystalofchocolate.picker.attachment.CollectedAttachment;
import pl.crystalofchocolate.picker.attachment.CollectedAttachmentDestination;
import pl.crystalofchocolate.picker.attachment.CollectedAttachmentMapper;
import pl.crystalofchocolate.util.sheduled.ScheduledUtil;

import javax.mail.*;
import javax.mail.event.MessageCountEvent;
import javax.mail.event.MessageCountListener;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;

@RequiredArgsConstructor
@Slf4j
public class PickerService implements MessageCountListener {
  @NonNull
  private final MailService mailService;
  @NonNull
  private final List<CollectedAttachmentDestination> collectedAttachmentDestinations;
  @NonNull
  private final ScheduledUtil scheduledUtil;

  @Override
  public void messagesAdded(MessageCountEvent messageCountEvent) {
    log.debug("==New message arrived==");
    log.debug(messageCountEvent.toString());
    for (Message countEventMessage : messageCountEvent.getMessages()) {
      MimeMessage inputMimeMessage = (MimeMessage) countEventMessage;
      List<MimeBodyPart> bodyParts = extractAttachments(inputMimeMessage);

      for (MimeBodyPart bodyPart : bodyParts) {
        try {
          if (isAttachmentWithName(bodyPart)) {
            CollectedAttachment collectedAttachment = CollectedAttachmentMapper.map(inputMimeMessage, bodyPart);
            callCollectsAttachment(collectedAttachment);
          }
        } catch (MessagingException e) {
          log.error("Incorrect mime message, failed to data access");
          log.debug("Messaging Exception : ", e);
        } catch (IOException e) {
          log.error("Unable to read attachment input stream or file name");
          log.debug("IO Exception : ", e);
        } catch (ParseException e) {
          log.error("Message date parse fail");
          log.debug("ParseException : ", e);
        }
      }
    }
  }

  private boolean isAttachmentWithName(MimeBodyPart bodyPart) throws MessagingException {
    return Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition())
        && Objects.nonNull(bodyPart.getFileName())
        && bodyPart.getFileName().length() > 0;
  }

  private List<MimeBodyPart> extractAttachments(MimeMessage message) {
    try {
      return mailService.extractAttachmentsFromMessage(message);
    } catch (Exception e) {
      log.debug("Cannot extract attachment", e);
      return Collections.emptyList();
    }
  }

  private void callCollectsAttachment(CollectedAttachment collectedAttachment) {
    log.debug(String.format("Input collected attachment : %s", collectedAttachment.getName()));

    for (CollectedAttachmentDestination destination : collectedAttachmentDestinations) {
      Runnable asyncSubmitAttachmentToDestination = () -> destination.collectAttachment(collectedAttachment);
      scheduledUtil.submit(asyncSubmitAttachmentToDestination);
    }
  }

  @Override
  public void messagesRemoved(MessageCountEvent messageCountEvent) {
    log.debug("Remove message is not used in this fantastic application");
  }
}
