package pl.crystalofchocolate;

import com.sun.mail.imap.IMAPFolder;
import lombok.extern.slf4j.Slf4j;
import pl.crystalofchocolate.config.ConfigLoader;
import pl.crystalofchocolate.config.AppProperties;
import pl.crystalofchocolate.config.PropertiesConfigLoader;
import pl.crystalofchocolate.exceptions.*;
import pl.crystalofchocolate.ftp.*;
import pl.crystalofchocolate.mail.MailListenerTriggerService;
import pl.crystalofchocolate.mail.MailService;
import pl.crystalofchocolate.picker.attachment.CollectedAttachmentDestination;
import pl.crystalofchocolate.picker.PickerService;
import pl.crystalofchocolate.storage.StoreService;
import pl.crystalofchocolate.util.AppConfiguration;
import pl.crystalofchocolate.util.ServiceBuilderUtil;
import pl.crystalofchocolate.util.sheduled.ScheduledUtil;

import javax.mail.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Executors;

@Slf4j
public class Main {

  public static void main(String[] args) {
    log.info("Starting attachment picker");
    log.debug("Starting with args :"+ List.of(args));
    if (args.length < 1) {
      throw new MissingConfigurationFileException();
    }

    if (!args[0].endsWith(".properties")) {
      throw new UnsupportedFileException(args[0]);
    }

    ConfigLoader configLoader = new PropertiesConfigLoader();
    AppProperties appProperties;
    try {
      appProperties = configLoader.loadConfigFromFile(args[0]);
    } catch (Exception e) {
      log.error("Cannot load configuration from file");
      log.debug("Error during load configuration", e);
      throw new IncorrectConfigurationFileException();
    }

    log.info("Configuration loaded");

    AppConfiguration appConfig = AppConfiguration.of(appProperties);
    MailService mailService;
    IMAPFolder inbox;
    try {
      mailService = ServiceBuilderUtil.buildMailService(appProperties);
      inbox = mailService.openMailStoreFolder(appConfig.getAppMailFolderName(), Folder.READ_WRITE);
    } catch (NoSuchProviderException e) {
      log.error("Cannot open mail store incorrect configuration or authentication failed");
      log.debug("Error during open mail store", e);
      throw new IncorrectMailBoxConfigurationException();
    } catch (MessagingException e) {
      log.error("INBOX catalogue not found");
      log.debug("Error during access to Inbox", e);
      throw new InboxNotFoundException();
    }

    log.info("Mail folder opened successfully");

    ScheduledUtil scheduledUtil = new ScheduledUtil(Executors.newScheduledThreadPool(appConfig.getAppTaskPoolSize()));

    List<CollectedAttachmentDestination> attachmentDestinations = new ArrayList<>();
    FTPService ftpService = ServiceBuilderUtil.buildFtpService(appProperties);
    StoreService storeService = ServiceBuilderUtil.buildStoreService(appProperties);

    attachmentDestinations.add(ServiceBuilderUtil.buildTelegramService(appProperties));
    attachmentDestinations.add(ServiceBuilderUtil.buildFtpDestinationService(ftpService, storeService, appProperties));

    PickerService pickerService = new PickerService(mailService, attachmentDestinations, scheduledUtil);
    inbox.addMessageCountListener(pickerService);

    log.info("Mail listener registered");
    MailListenerTriggerService mailListenerTriggerService = MailListenerTriggerService.builder()
        .imapInboxFolder(inbox)
        .delayTimeValue(appConfig.getAppMailCheckTime())
        .delayTimeUnit(appConfig.getAppMailCheckTimeUnit())
        .build();

    FTPLocalFileLoaderService ftpLocalFileLoaderService = FTPLocalFileLoaderService.builder()
        .ftpService(ftpService)
        .storeService(storeService)
        .delayTimeValue(appConfig.getAppFtpCheckTime())
        .delayTimeUnit(appConfig.getAppFtpCheckTimeUnit())
        .build();

    scheduledUtil.schedule(ftpLocalFileLoaderService);
    scheduledUtil.schedule(mailListenerTriggerService);
    log.info("Attachment Picker Online");
    log.info("Enter \"exit\" to close this fantastic app");
    log.info("Or enter \"upload\" to manually start task that store local files on ftp server");

    boolean keepAlive = true;
    Scanner commandScanner = new Scanner(System.in);
    while (keepAlive) {

      String command = commandScanner.nextLine();
      if (command.equalsIgnoreCase("exit")) {
        keepAlive = false;
      } else if (command.equalsIgnoreCase("upload")) {
        scheduledUtil.schedule(ftpLocalFileLoaderService);
      }
    }

    try {
      inbox.close();
    } catch (MessagingException e) {
      log.debug("Failed during closing inbox", e);
    }

    scheduledUtil.close();
    ftpService.close();
    log.info("Attachment Picker was closed. Bye.");
  }
}