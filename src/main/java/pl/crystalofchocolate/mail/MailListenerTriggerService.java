package pl.crystalofchocolate.mail;

import com.sun.mail.imap.IMAPFolder;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.crystalofchocolate.util.sheduled.ScheduledTask;

import javax.mail.MessagingException;
import java.util.concurrent.TimeUnit;

@RequiredArgsConstructor
@Slf4j
@Builder
public class MailListenerTriggerService implements ScheduledTask {
  @NonNull
  private final IMAPFolder imapInboxFolder;
  @NonNull
  @Getter
  private final Long delayTimeValue;
  @NonNull
  @Getter
  private final TimeUnit delayTimeUnit;

  @Override
  public void run() {
    if (imapInboxFolder.isOpen()) {
      try {
        imapInboxFolder.getMessageCount();
      } catch (MessagingException e) {
        log.debug("Error during call message count from imap inbox folder");
      }
    }
  }
}
