package pl.crystalofchocolate.mail.repository;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import pl.crystalofchocolate.config.AppProperties;

import java.util.Objects;
import java.util.Properties;

@Builder
@Value
public class MailServerConnection {
  @NonNull
  String host;
  @NonNull
  int port;
  @Builder.Default
  String protocol = "imap";
  @Builder.Default
  boolean ssl = true;
  @NonNull
  String user;
  @NonNull
  String password;

  public Properties toSessionProperties() {
    Properties sessionProperties = new Properties();
    sessionProperties.setProperty("mail.store.protocol", protocol);
    sessionProperties.setProperty("mail.imap.host", host);
    sessionProperties.setProperty("mail.imap.port", String.valueOf(port));
    sessionProperties.setProperty("mail.imap.ssl.enable", String.valueOf(ssl));
    return sessionProperties;
  }

  public static MailServerConnection of(AppProperties config) {
    return MailServerConnection.builder()
        .host(config.getMailHost())
        .port(Integer.parseInt(config.getMailPort()))
        .user(config.getMailUser())
        .password(config.getMailPassword())
        .ssl(Objects.nonNull(config.getMailSSL()) ? Boolean.parseBoolean(config.getMailSSL()) : $default$ssl())
        .protocol(Objects.nonNull(config.getMailProtocol()) ? config.getMailProtocol() : $default$protocol())
        .build();
  }

}
