package pl.crystalofchocolate.mail.repository;

import com.sun.mail.imap.IMAPFolder;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;

@Slf4j
public class MailRepository implements AutoCloseable {
  private final MailServerConnection mailServerConnection;
  private final Store store;
  private IMAPFolder imapFolder;
  @Getter
  private final Session session;

  public MailRepository(@NonNull MailServerConnection mailServerConnection) throws NoSuchProviderException {
    this.mailServerConnection = mailServerConnection;
    session = Session.getDefaultInstance(mailServerConnection.toSessionProperties(), null);
    store = session.getStore();
  }

  MailRepository(@NonNull MailServerConnection mailServerConnection, Session session) throws NoSuchProviderException {
    this.mailServerConnection = mailServerConnection;
    this.session = session;
    store = session.getStore();
  }

  public IMAPFolder getMailStoreFolder(String folderMailName) throws MessagingException {
    store.connect(mailServerConnection.getUser(), mailServerConnection.getPassword());
    imapFolder = (IMAPFolder) store.getFolder(folderMailName);
    log.debug(String.format("Open mail store folder %s for user %s", folderMailName, mailServerConnection.getUser()));
    return imapFolder;
  }

  @Override
  public void close() throws Exception {
    imapFolder.close();
    store.close();
    log.debug("Closed mail repository");
  }
}
