package pl.crystalofchocolate.mail;

import com.sun.mail.imap.IMAPFolder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.crystalofchocolate.mail.repository.MailRepository;

import javax.mail.*;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Slf4j
public class MailService {
  @NonNull
  private final MailRepository mailRepository;

  public IMAPFolder openMailStoreFolder(String folderMailName, int folderOpenMode) throws MessagingException {
    IMAPFolder imapFolder = mailRepository.getMailStoreFolder(folderMailName);
    imapFolder.open(folderOpenMode);
    return imapFolder;
  }

  public List<MimeBodyPart> extractAttachmentsFromMessage(MimeMessage inputMessage) throws IOException, MessagingException {
    MimeMultipart inputMessageMultipart = (MimeMultipart) inputMessage.getContent();
    List<MimeBodyPart> bodyPartsList = new ArrayList<>(inputMessageMultipart.getCount());

    for (int multipartIndex = 0; multipartIndex < inputMessageMultipart.getCount(); multipartIndex++) {
      bodyPartsList.add((MimeBodyPart) inputMessageMultipart.getBodyPart(multipartIndex));
    }
    log.debug(String.format("Extracted attachment count : %d", bodyPartsList.size()));
    return bodyPartsList;
  }
}
