package pl.crystalofchocolate.telegram.filter;

import lombok.*;
import pl.crystalofchocolate.util.filter.Filter;

import java.util.List;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class TelegramFilter extends Filter {

  @Getter
  @Setter
  private List<Integer> chatsIds;
  @Getter
  @Setter
  private String message;

  public TelegramFilter(@NonNull String filterName) {
    super(filterName);
  }
}
