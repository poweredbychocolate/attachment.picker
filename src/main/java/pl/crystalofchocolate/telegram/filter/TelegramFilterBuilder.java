package pl.crystalofchocolate.telegram.filter;

import lombok.extern.slf4j.Slf4j;
import pl.crystalofchocolate.config.AppProperties;
import pl.crystalofchocolate.util.filter.FilterBuilder;

import static pl.crystalofchocolate.util.NameUtil.TELEGRAM_FILTER_KEY_PREFIX;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public class TelegramFilterBuilder extends FilterBuilder {

  public List<TelegramFilter> buildTelegramFilters(AppProperties config) {
    List<TelegramFilter> telegramFilters = new ArrayList<>();

    if (Objects.isNull(config.getConfigProperties()) || config.getConfigProperties().isEmpty()) {
      return telegramFilters;
    }

    Map<String, String> propertiesMap = propertiesToMap(config);
    List<String> foundedKeyNames = findKeysNames(propertiesMap);

    for (String foundedName : foundedKeyNames) {
      Map<String, String> propertiesForFoundedName = findEntriesForKeyName(foundedName, propertiesMap);
      TelegramFilter telegramFilter = buildTelegramFilter(foundedName, propertiesForFoundedName);
      telegramFilters.add(telegramFilter);
    }

    log.debug(String.format("Filters size : %d", telegramFilters.size()));
    return telegramFilters;
  }

  private TelegramFilter buildTelegramFilter(String foundedName, Map<String, String> filterProperties) {
    TelegramFilter telegramFilter = new TelegramFilter(foundedName);
    for (Map.Entry<String, String> nameEntry : filterProperties.entrySet()) {

      if (nameEntry.getKey().endsWith(".author")) {
        telegramFilter.setAuthors(extractPatterns(nameEntry.getValue()));
      } else if (nameEntry.getKey().endsWith(".file.name")) {
        telegramFilter.setFilesNames(extractPatterns(nameEntry.getValue()));
      } else if (nameEntry.getKey().endsWith(".subject")) {
        telegramFilter.setSubjects(extractPatterns(nameEntry.getValue()));
      } else if (nameEntry.getKey().endsWith(".file.extension")) {
        telegramFilter.setFilesExtensions(extractPatterns(nameEntry.getValue()));
      } else if (nameEntry.getKey().endsWith(".message")) {
        telegramFilter.setMessage(nameEntry.getValue());
      } else if (nameEntry.getKey().endsWith(".channels")) {
        telegramFilter.setChatsIds(extractChannelsIds(nameEntry.getValue()));
      }
    }

    log.debug(String.format("Built Filter : %s",telegramFilter));
    return telegramFilter;
  }

  private List<Integer> extractChannelsIds(String value) {
    return Arrays.stream(value.split("[,]"))
        .map(Integer::parseInt)
        .collect(Collectors.toList());
  }

  @Override
  protected String getFilterKeyPrefix() {
    return TELEGRAM_FILTER_KEY_PREFIX;
  }
}
