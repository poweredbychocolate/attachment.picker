package pl.crystalofchocolate.telegram.repository;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.request.AbstractSendRequest;
import com.pengrad.telegrambot.response.SendResponse;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

@Slf4j
@AllArgsConstructor
public class TelegramRepository {
  @NonNull
  private final TelegramConnection telegramConnection;
  private final TelegramBot telegramBot;

  public TelegramRepository(@NonNull TelegramConnection telegramConnection) {
    this.telegramConnection = telegramConnection;
    this.telegramBot = new TelegramBot(telegramConnection.getToken());
  }

  public boolean sendMessage(AbstractSendRequest<?> sendMessage) {
    log.debug(String.format("Sending message to channel : %s", sendMessage.getParameters().get("chat_id")));
    SendResponse sendResponse = telegramBot.execute(sendMessage);
    log.debug(String.format("Message sent to channel execute state : %s", sendResponse.isOk()));
    log.debug(String.format("ERROR CODE : %d", sendResponse.errorCode()));
    log.debug(String.format("Description : %s", sendResponse.description()));
    return sendResponse.isOk();
  }

  public boolean isSendToDefaultChannelEnabled() {
    return Objects.nonNull(telegramConnection.getDefaultChanelId());
  }

  public Integer getDefaultChannelId() {
    return telegramConnection.getDefaultChanelId();
  }
}
