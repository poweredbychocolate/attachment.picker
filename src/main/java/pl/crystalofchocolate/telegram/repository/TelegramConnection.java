package pl.crystalofchocolate.telegram.repository;

import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import pl.crystalofchocolate.config.AppProperties;

import java.util.Objects;

@Value
@RequiredArgsConstructor
@Builder
public class TelegramConnection {
  @NonNull
  String token;
  Integer defaultChanelId;

  public static TelegramConnection of(AppProperties config) {
    return TelegramConnection.builder()
        .token(config.getTelegramToken())
        .defaultChanelId(getDefaultChannelId(config))
        .build();
  }

  private static Integer getDefaultChannelId(AppProperties config) {
    return Objects.nonNull(config.getTelegramDefaultChannelId())
        ? Integer.parseInt(config.getTelegramDefaultChannelId())
        : null;
  }
}
