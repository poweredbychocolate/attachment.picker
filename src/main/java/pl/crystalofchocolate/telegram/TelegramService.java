package pl.crystalofchocolate.telegram;

import com.pengrad.telegrambot.model.request.ParseMode;
import com.pengrad.telegrambot.request.SendDocument;
import com.pengrad.telegrambot.request.SendMessage;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.crystalofchocolate.picker.attachment.CollectedAttachment;
import pl.crystalofchocolate.picker.attachment.CollectedAttachmentDestination;
import pl.crystalofchocolate.telegram.filter.TelegramFilter;
import pl.crystalofchocolate.telegram.repository.TelegramRepository;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
public class TelegramService implements CollectedAttachmentDestination {
  @NonNull
  private final TelegramRepository telegramRepository;
  @NonNull
  private final List<TelegramFilter> telegramFilters;

  @Override
  public void collectAttachment(CollectedAttachment collectedAttachment) {
    boolean anyMatch = false;

    for (TelegramFilter telegramFilter : telegramFilters) {
      if (telegramFilter.match(collectedAttachment)) {
        log.debug(String.format("attachment %s caught by %s telegram filter", collectedAttachment.getName(), telegramFilter.getFilterName()));

        for (Integer filterChatId : telegramFilter.getChatsIds()) {
          synchronized (this) {
            Optional<SendMessage> messageHeader = buildTextMessage(filterChatId, telegramFilter.getMessage());
            messageHeader.ifPresent(telegramRepository::sendMessage);
            SendDocument sendAttachmentDocument = buildDocumentMessage(filterChatId, collectedAttachment);
            telegramRepository.sendMessage(sendAttachmentDocument);
          }
        }
        anyMatch = true;
      }
    }

    if (!anyMatch && telegramRepository.isSendToDefaultChannelEnabled()) {
      Integer defaultChannelId = telegramRepository.getDefaultChannelId();
      SendDocument sendAttachmentDocument = buildDocumentMessage(defaultChannelId, collectedAttachment);
      telegramRepository.sendMessage(sendAttachmentDocument);
    }
  }

  private Optional<SendMessage> buildTextMessage(Integer chatId, String message) {
    if (Objects.nonNull(message)) {
      SendMessage sendMessage = new SendMessage(chatId, message)
          .parseMode(ParseMode.HTML);

      return Optional.of(sendMessage);
    }
    return Optional.empty();
  }

  private SendDocument buildDocumentMessage(Integer filterChatId, CollectedAttachment collectedAttachment) {
    return new SendDocument(filterChatId, collectedAttachment.getContent())
        .parseMode(ParseMode.HTML)
        .fileName(collectedAttachment.getName());
  }
}
