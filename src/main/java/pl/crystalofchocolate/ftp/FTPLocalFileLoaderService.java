package pl.crystalofchocolate.ftp;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.crystalofchocolate.picker.attachment.CollectedAttachment;
import pl.crystalofchocolate.storage.StoreService;
import pl.crystalofchocolate.util.sheduled.ScheduledTask;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static pl.crystalofchocolate.util.NameUtil.*;

@RequiredArgsConstructor
@Slf4j
@Builder
public class FTPLocalFileLoaderService implements ScheduledTask {
  @NonNull
  private final FTPService ftpService;
  @NonNull
  private final StoreService storeService;
  @NonNull
  @Getter
  private final Long delayTimeValue;
  @NonNull
  @Getter
  private final TimeUnit delayTimeUnit;

  @Override
  public void run() {

    if (ftpService.isOnline()) {
      List<String> localFilesNames = loadFilesNames();

      for (String fileName : localFilesNames) {
        try {
          Properties attachmentProperties = storeService.fetchAttachmentProperties(LOCAL_FTP_SUBDIRECTORY, fileName);
          LocalDateTime receivedDateTime = stringToLocalDateTime(attachmentProperties.getProperty(FTP_DATE));

          CollectedAttachment restoredAttachment = restoreAttachment(fileName, receivedDateTime);
          String subdirectoryName = attachmentProperties.getProperty(FTP_SUBDIRECTORY);

          ftpService.storeAttachment(subdirectoryName, restoredAttachment);
          storeService.removeAttachment(fileName, LOCAL_FTP_SUBDIRECTORY);
        } catch (IOException e) {
          log.error("Exception during file processing");
          log.debug("File processing exception", e);
        }
      }

    } else {
      log.debug("Do nothing, ftp server is still offline");
    }
  }

  private CollectedAttachment restoreAttachment(String fileName, LocalDateTime receivedDateTime) throws IOException {
    byte[] attachmentContent = storeService.fetchAttachmentContent(LOCAL_FTP_SUBDIRECTORY, fileName);

    return CollectedAttachment.builder()
        .name(fileName)
        .content(attachmentContent)
        .receivedDate(receivedDateTime)
        .build();
  }

  private List<String> loadFilesNames() {
    try {
      return storeService.getFilesList(LOCAL_FTP_SUBDIRECTORY)
          .stream()
          .filter(fileName -> !fileName.endsWith(PROPERTIES_SUFFIX))
          .collect(Collectors.toList());
    } catch (IOException e) {
      log.debug("Error when try load flies names stored locally", e);
      return Collections.emptyList();
    }
  }

}
