package pl.crystalofchocolate.ftp;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.crystalofchocolate.ftp.filter.FTPFilter;
import pl.crystalofchocolate.picker.attachment.CollectedAttachment;
import pl.crystalofchocolate.picker.attachment.CollectedAttachmentDestination;

import static pl.crystalofchocolate.util.NameUtil.localDateTimeToString;
import static pl.crystalofchocolate.util.NameUtil.FTP_SUBDIRECTORY;
import static pl.crystalofchocolate.util.NameUtil.FTP_DATE;
import static pl.crystalofchocolate.util.NameUtil.LOCAL_FTP_SUBDIRECTORY;

import pl.crystalofchocolate.storage.StoreService;

import java.io.IOException;
import java.net.NoRouteToHostException;
import java.util.List;
import java.util.Properties;

@RequiredArgsConstructor
@Slf4j
public class FTPDestinationService implements CollectedAttachmentDestination {

  @NonNull
  private final FTPService ftpService;
  @NonNull
  private final StoreService storeService;
  @NonNull
  private final List<FTPFilter> ftpFilterList;

  @Override
  public void collectAttachment(CollectedAttachment collectedAttachment) {
    for (FTPFilter ftpFilter : ftpFilterList) {
      storeAttachmentOnFilterMatch(collectedAttachment, ftpFilter);
    }
  }

  private void storeAttachmentOnFilterMatch(CollectedAttachment collectedAttachment, FTPFilter ftpFilter) {
    if (ftpFilter.match(collectedAttachment)) {
      log.debug(String.format("attachment %s caught by %s ftp filter", collectedAttachment.getName(), ftpFilter.getFilterName()));
      try {
        ftpService.storeAttachment(ftpFilter.getStoreSubdirectory(), collectedAttachment);
      } catch (NoRouteToHostException e) {
        log.error("Cannot store attachment. Ftp is not reachable for now.");
        log.debug("Ftp sever is unreachable", e);
        ftpService.close();
        onStoreAttachmentInSubdirectoryFail(collectedAttachment, ftpFilter);
      } catch (IOException e) {
        log.error("Cannot store attachment, error during upload file");
        log.debug("Error during upload file to ftp server", e);
        onStoreAttachmentInSubdirectoryFail(collectedAttachment, ftpFilter);
      }
      log.debug(String.format("Match %s by %s filter", collectedAttachment.getName(), ftpFilter.getFilterName()));
    }
  }

  private void onStoreAttachmentInSubdirectoryFail(CollectedAttachment collectedAttachment, FTPFilter ftpFilter) {
    Properties properties = prepareProperties(collectedAttachment);
    properties.put(FTP_SUBDIRECTORY, ftpFilter.getStoreSubdirectory());
    tryStoreFileToLocalDictionary(collectedAttachment, properties);
  }

  private Properties prepareProperties(CollectedAttachment collectedAttachment) {
    Properties properties = new Properties();
    properties.put(FTP_DATE, localDateTimeToString(collectedAttachment.getReceivedDate()));
    return properties;
  }

  private void tryStoreFileToLocalDictionary(CollectedAttachment collectedAttachment, Properties properties) {
    try {
      storeService.storeAttachment(collectedAttachment, LOCAL_FTP_SUBDIRECTORY, properties);
    } catch (IOException e) {
      log.error("Error during saving to local store");
    }
  }
}
