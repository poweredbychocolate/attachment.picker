package pl.crystalofchocolate.ftp.filter;

import lombok.*;
import pl.crystalofchocolate.util.filter.Filter;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class FTPFilter extends Filter {

  @Getter
  @Setter
  private String storeSubdirectory;

  public FTPFilter(@NonNull String filterName) {
    super(filterName);
  }
}
