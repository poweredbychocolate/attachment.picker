package pl.crystalofchocolate.ftp.filter;

import lombok.extern.slf4j.Slf4j;
import pl.crystalofchocolate.config.AppProperties;
import pl.crystalofchocolate.util.filter.FilterBuilder;

import static pl.crystalofchocolate.util.NameUtil.FTP_FILTER_KEY_PREFIX;

import java.util.*;

@Slf4j
public class FTPFilterBuilder extends FilterBuilder {

  public List<FTPFilter> buildFtpFilters(AppProperties config) {
    List<FTPFilter> ftpFilters = new ArrayList<>();

    if (Objects.isNull(config.getConfigProperties()) || config.getConfigProperties().isEmpty()) {
      return ftpFilters;
    }

    Map<String, String> propertiesMap = propertiesToMap(config);
    List<String> foundedKeyNames = findKeysNames(propertiesMap);

    for (String foundedName : foundedKeyNames) {
      Map<String, String> propertiesForFoundedName = findEntriesForKeyName(foundedName, propertiesMap);
      FTPFilter ftpFilter = buildFtpFilter(foundedName, propertiesForFoundedName);
      ftpFilters.add(ftpFilter);
    }
    log.debug(String.format("Filters size : %d", ftpFilters.size()));
    return ftpFilters;
  }

  private FTPFilter buildFtpFilter(String foundedName, Map<String, String> filterProperties) {
    FTPFilter ftpFilter = new FTPFilter(foundedName);
    for (Map.Entry<String, String> nameEntry : filterProperties.entrySet()) {

      if (nameEntry.getKey().endsWith(".author")) {
        ftpFilter.setAuthors( extractPatterns(nameEntry.getValue()));
      } else if (nameEntry.getKey().endsWith(".file.name")) {
        ftpFilter.setFilesNames(extractPatterns(nameEntry.getValue()));
      } else if (nameEntry.getKey().endsWith(".subject")) {
        ftpFilter.setSubjects(extractPatterns(nameEntry.getValue()));
      }else if (nameEntry.getKey().endsWith(".file.extension")) {
        ftpFilter.setFilesExtensions(extractPatterns(nameEntry.getValue()));
      }else if (nameEntry.getKey().endsWith(".store.directory")) {
        ftpFilter.setStoreSubdirectory(nameEntry.getValue());
      }
    }

    log.debug(String.format("Built Filter : %s", ftpFilter));
    return ftpFilter;
  }

  @Override
  protected String getFilterKeyPrefix() {
    return FTP_FILTER_KEY_PREFIX;
  }
}
