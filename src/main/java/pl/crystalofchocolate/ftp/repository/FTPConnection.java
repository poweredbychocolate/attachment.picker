package pl.crystalofchocolate.ftp.repository;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import pl.crystalofchocolate.config.AppProperties;

import java.util.Objects;

@Value
@Builder
public class FTPConnection {
  @NonNull
  String address;
  @NonNull
  String user;
  @NonNull
  String password;
  @NonNull
  String dictionary;
  @Builder.Default
  Integer port=21;


  public static FTPConnection of(AppProperties config) {
    return FTPConnection.builder()
        .address(config.getFtpAddress())
        .user(config.getFtpUser())
        .password(config.getFtpPassword())
        .dictionary(config.getFtpDictionary())
        .port(Objects.nonNull(config.getFtpPort())?Integer.parseInt(config.getFtpPort()):$default$port())
        .build();
  }
}
