package pl.crystalofchocolate.ftp.repository;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

@Slf4j
public class FTPRepository implements AutoCloseable {
  @NonNull
  private final FTPConnection ftpConnection;
  private final FTPClient ftpClient;

  public FTPRepository(FTPConnection ftpConnection) {
    this.ftpConnection = ftpConnection;
    ftpClient = new FTPClient();
  }

  private void reconnect() throws IOException {
    if (!ftpClient.isConnected()) {
      ftpClient.setControlEncoding(StandardCharsets.UTF_8.name());
      ftpClient.setAutodetectUTF8(true);

      ftpClient.connect(ftpConnection.getAddress(), ftpConnection.getPort());

      if (!FTPReply.isPositiveCompletion(ftpClient.getReplyCode())) {
        ftpClient.disconnect();
        log.warn(String.format("Cannot reconnect to ftp server %s", ftpConnection.getAddress()));
        return;
      }

      ftpClient.login(ftpConnection.getUser(), ftpConnection.getPassword());
      ftpClient.enterLocalPassiveMode();
      ftpClient.setFileType(FTP.BINARY_FILE_TYPE, FTP.BINARY_FILE_TYPE);
      ftpClient.setFileTransferMode(FTP.BINARY_FILE_TYPE);
      log.debug("Reconnected to ftp server successfully");
    }
  }

  public synchronized void switchToSubdirectory(String subdirectory) throws IOException {
    reconnect();
    ftpClient.changeWorkingDirectory(ftpConnection.getDictionary() + subdirectory);
    log.debug(String.format("Switch to subdirectory %s", subdirectory));
  }

  public synchronized void storeFile(String fileName, InputStream fileInputStream) throws IOException {
    reconnect();
    ftpClient.storeFile(fileName, fileInputStream);
    log.debug(String.format("Store file : %s", fileName));
  }

  public synchronized boolean isFileExist(String fileName) {
    try {
      reconnect();
      List<String> filesNames = Arrays.asList(ftpClient.listNames());
      log.debug(String.format("Founded names : %s", filesNames));
      return filesNames.contains(fileName);
    } catch (IOException e) {
      log.debug("Exception during check if file exist", e);
      return false;
    }
  }

  @Override
  public synchronized void close() {
    try {
      if (ftpClient.isConnected()) {
        ftpClient.disconnect();
      }
      log.debug("FTPRepository disconnect from server");
    } catch (IOException e) {
      log.debug("FTPRepository disconnected nothing to do");
    }
  }

  public synchronized boolean isOnline() {
    try {
      reconnect();
      return ftpClient.isConnected();
    } catch (IOException e) {
      log.error("Server is offline, cannot reconnect");
      return false;
    }
  }
}
