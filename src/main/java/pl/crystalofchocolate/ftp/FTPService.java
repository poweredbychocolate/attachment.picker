package pl.crystalofchocolate.ftp;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.crystalofchocolate.ftp.repository.FTPRepository;
import pl.crystalofchocolate.picker.attachment.CollectedAttachment;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static pl.crystalofchocolate.util.NameUtil.*;

@RequiredArgsConstructor
@Slf4j
public class FTPService implements AutoCloseable {
  @NonNull
  private final FTPRepository ftpRepository;

  void storeAttachment(String storeSubdirectory, CollectedAttachment collectedAttachment) throws IOException {
    ftpRepository.switchToSubdirectory(storeSubdirectory);
    storeFile(collectedAttachment);
  }

  private void storeFile(CollectedAttachment collectedAttachment) throws IOException {
    if (ftpRepository.isFileExist(collectedAttachment.getName())) {
      String fileName = prepareFileNameWithDate(collectedAttachment);
      ftpRepository.storeFile(fileName, getContentAsInputStream(collectedAttachment));
      log.debug(String.format("Store attachment[%s] with date prefix", collectedAttachment.getName()));
    } else {
      ftpRepository.storeFile(collectedAttachment.getName(), getContentAsInputStream(collectedAttachment));
      log.debug(String.format("Store attachment[%s]", collectedAttachment.getName()));
    }
  }

  private InputStream getContentAsInputStream(CollectedAttachment collectedAttachment) {
    return new ByteArrayInputStream(collectedAttachment.getContent());
  }

  @Override
  public void close() {
    ftpRepository.close();
  }

  public boolean isOnline() {
    return ftpRepository.isOnline();
  }

}
