package pl.crystalofchocolate.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.crystalofchocolate.config.AppProperties;
import pl.crystalofchocolate.ftp.FTPDestinationService;
import pl.crystalofchocolate.ftp.FTPService;
import pl.crystalofchocolate.ftp.filter.FTPFilter;
import pl.crystalofchocolate.ftp.filter.FTPFilterBuilder;
import pl.crystalofchocolate.ftp.repository.FTPConnection;
import pl.crystalofchocolate.ftp.repository.FTPRepository;
import pl.crystalofchocolate.mail.MailService;
import pl.crystalofchocolate.mail.repository.MailRepository;
import pl.crystalofchocolate.mail.repository.MailServerConnection;
import pl.crystalofchocolate.storage.StoreService;
import pl.crystalofchocolate.storage.repository.StoreConnection;
import pl.crystalofchocolate.storage.repository.StoreRepository;
import pl.crystalofchocolate.telegram.TelegramService;
import pl.crystalofchocolate.telegram.filter.TelegramFilter;
import pl.crystalofchocolate.telegram.filter.TelegramFilterBuilder;
import pl.crystalofchocolate.telegram.repository.TelegramConnection;
import pl.crystalofchocolate.telegram.repository.TelegramRepository;

import javax.mail.NoSuchProviderException;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ServiceBuilderUtil {

  public static TelegramService buildTelegramService(AppProperties appProperties) {
    TelegramRepository telegramRepository = new TelegramRepository(TelegramConnection.of(appProperties));

    TelegramFilterBuilder telegramFilterBuilder = new TelegramFilterBuilder();
    List<TelegramFilter> telegramFilters = telegramFilterBuilder.buildTelegramFilters(appProperties);

    return new TelegramService(telegramRepository, telegramFilters);
  }


  public static FTPService buildFtpService(AppProperties config) {
    FTPRepository ftpRepository = new FTPRepository(FTPConnection.of(config));
    return new FTPService(ftpRepository);
  }

  public static StoreService buildStoreService(AppProperties config) {
    StoreRepository storeRepository = new StoreRepository(StoreConnection.of(config));
    return new StoreService(storeRepository);
  }

  public static FTPDestinationService buildFtpDestinationService(FTPService ftpService, StoreService storeService, AppProperties config) {
    FTPFilterBuilder ftpFilterBuilder = new FTPFilterBuilder();
    List<FTPFilter> ftpFilterList = ftpFilterBuilder.buildFtpFilters(config);
    return new FTPDestinationService(ftpService, storeService, ftpFilterList);
  }

  public static MailService buildMailService(AppProperties appProperties) throws NoSuchProviderException {
    MailRepository mailRepository = new MailRepository(MailServerConnection.of(appProperties));
    return new MailService(mailRepository);
  }
}
