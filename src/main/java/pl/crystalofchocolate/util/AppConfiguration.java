package pl.crystalofchocolate.util;

import lombok.Builder;
import lombok.Value;
import pl.crystalofchocolate.config.AppProperties;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Value
@Builder
public class AppConfiguration {
  @Builder.Default
  String appMailFolderName = "Inbox";
  @Builder.Default
  int appTaskPoolSize = 4;
  @Builder.Default
  long appFtpCheckTime = 10L;
  @Builder.Default
  TimeUnit appFtpCheckTimeUnit = TimeUnit.MINUTES;
  @Builder.Default
  long appMailCheckTime = 1L;
  @Builder.Default
  TimeUnit appMailCheckTimeUnit = TimeUnit.MINUTES;

  public static AppConfiguration of(AppProperties appProperties) {
    return AppConfiguration.builder()
        .appMailFolderName(prepareMailFolderName(appProperties))
        .appTaskPoolSize(prepareTasksPoolSize(appProperties))
        .appMailCheckTime(prepareMailCheckTime(appProperties))
        .appFtpCheckTime(prepareFtpCheckTime(appProperties))
        .appMailCheckTimeUnit(prepareMailCheckTimeUnit(appProperties))
        .appFtpCheckTimeUnit(prepareFtpCheckTimeUnit(appProperties))
        .build();
  }

  private static String prepareMailFolderName(AppProperties appProperties) {
    return Objects.nonNull(appProperties.getAppMailFolderName())
        ? appProperties.getAppMailFolderName()
        : $default$appMailFolderName();
  }

  private static int prepareTasksPoolSize(AppProperties appProperties) {
    return Objects.nonNull(appProperties.getAppTaskPoolSize())
        ? Integer.parseInt(appProperties.getAppTaskPoolSize())
        : $default$appTaskPoolSize();
  }

  private static long prepareMailCheckTime(AppProperties appProperties) {
    return Objects.nonNull(appProperties.getAppMailCheckTime())
        ? Long.parseLong(appProperties.getAppMailCheckTime())
        : $default$appMailCheckTime();
  }

  private static long prepareFtpCheckTime(AppProperties appProperties) {
    return Objects.nonNull(appProperties.getAppFtpCheckTime())
        ? Long.parseLong(appProperties.getAppFtpCheckTime())
        : $default$appFtpCheckTime();
  }

  private static TimeUnit prepareMailCheckTimeUnit(AppProperties appProperties) {
    return Objects.nonNull(appProperties.getAppMailCheckTimeUnit())
        ? TimeUnit.valueOf(appProperties.getAppMailCheckTimeUnit())
        : $default$appMailCheckTimeUnit();
  }

  private static TimeUnit prepareFtpCheckTimeUnit(AppProperties appProperties) {
    return Objects.nonNull(appProperties.getAppFtpCheckTimeUnit())
        ? TimeUnit.valueOf(appProperties.getAppFtpCheckTimeUnit())
        : $default$appFtpCheckTimeUnit();
  }
}
