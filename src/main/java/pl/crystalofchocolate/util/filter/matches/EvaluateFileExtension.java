package pl.crystalofchocolate.util.filter.matches;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EvaluateFileExtension {

  public static boolean evaluateFilesExtensions(List<String> filterFilesExtensionsPatterns, String collectedAttachmentName) {

    if (Objects.nonNull(filterFilesExtensionsPatterns)) {
      return filterFilesExtensionsPatterns.stream()
          .map(extensionPattern -> evaluateFileExtension(extensionPattern, collectedAttachmentName))
          .filter(Boolean::booleanValue)
          .findAny()
          .orElse(Boolean.FALSE);
    }
    return true;
  }

  private static boolean evaluateFileExtension(String filterFileExtensionPattern, String collectedAttachmentName) {
    return collectedAttachmentName.endsWith("." + filterFileExtensionPattern);
  }
}
