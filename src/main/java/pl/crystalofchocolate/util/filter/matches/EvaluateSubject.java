package pl.crystalofchocolate.util.filter.matches;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EvaluateSubject {

  public static boolean evaluateSubjects(List<String> filterSubjectsPatterns, String collectedAttachmentSubject) {

    if (Objects.nonNull(filterSubjectsPatterns)) {
      return filterSubjectsPatterns.stream()
          .map(subjectPattern -> evaluateSubject(subjectPattern, collectedAttachmentSubject))
          .filter(Boolean::booleanValue)
          .findAny()
          .orElse(Boolean.FALSE);
    }
    return true;
  }

  private static boolean evaluateSubject(String filterSubjectPattern, String collectedAttachmentSubject) {
    return Objects.nonNull(collectedAttachmentSubject)
        && collectedAttachmentSubject.contains(filterSubjectPattern);
  }
}
