package pl.crystalofchocolate.util.filter.matches;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EvaluateFileName {

  public static boolean evaluateFilesNames(List<String> filterFilesNamesPatterns, String collectedAttachmentName) {

    if (Objects.nonNull(filterFilesNamesPatterns)) {
      return filterFilesNamesPatterns.stream()
          .map(namePattern -> evaluateFilesNames(namePattern, collectedAttachmentName))
          .filter(Boolean::booleanValue)
          .findAny()
          .orElse(Boolean.FALSE);
    }
    return true;
  }

  private static boolean evaluateFilesNames(String filterFileNamePattern, String collectedAttachmentName) {
    return Objects.nonNull(collectedAttachmentName)
        && collectedAttachmentName.startsWith(filterFileNamePattern);
  }
}
