package pl.crystalofchocolate.util.filter.matches;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EvaluateAuthor {

  public static boolean evaluateAuthors(List<String> filterAuthorsPatterns, String collectedAttachmentAuthor) {

    if (Objects.nonNull(filterAuthorsPatterns)) {
      return filterAuthorsPatterns.stream()
          .map(authorPattern -> evaluateAuthor(authorPattern, collectedAttachmentAuthor))
          .filter(Boolean::booleanValue)
          .findAny()
          .orElse(Boolean.FALSE);
    }
    return true;
  }

  private static boolean evaluateAuthor(String filterAuthorPattern, String collectedAttachmentAuthor) {
    return Objects.nonNull(collectedAttachmentAuthor)
        && collectedAttachmentAuthor.contains(filterAuthorPattern);
  }
}
