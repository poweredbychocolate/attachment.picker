package pl.crystalofchocolate.util.filter;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.crystalofchocolate.picker.attachment.CollectedAttachment;

import java.util.List;

import static pl.crystalofchocolate.util.filter.matches.EvaluateAuthor.evaluateAuthors;
import static pl.crystalofchocolate.util.filter.matches.EvaluateFileExtension.evaluateFilesExtensions;
import static pl.crystalofchocolate.util.filter.matches.EvaluateFileName.evaluateFilesNames;
import static pl.crystalofchocolate.util.filter.matches.EvaluateSubject.evaluateSubjects;

@Data
@RequiredArgsConstructor
@Slf4j
public class Filter {
  @NonNull
  private String filterName;
  private List<String> filesNames;
  private List<String> filesExtensions;
  private List<String> subjects;
  private List<String> authors;


  public boolean match(CollectedAttachment collectedAttachment) {
    return evaluateSubjects(getSubjects(),collectedAttachment.getWithSubject())
        && evaluateFilesNames(getFilesNames(),collectedAttachment.getName())
        && evaluateFilesExtensions(getFilesExtensions(),collectedAttachment.getName())
        && evaluateAuthors(getAuthors(),collectedAttachment.getFromAddress());
  }
}
