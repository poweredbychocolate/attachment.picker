package pl.crystalofchocolate.util.filter;

import pl.crystalofchocolate.config.AppProperties;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class FilterBuilder {

  protected Map<String, String> propertiesToMap(AppProperties config) {
    return config.getConfigProperties()
        .entrySet()
        .stream()
        .collect(Collectors.toMap(this::mapKey, this::mapValue));
  }

  private String mapKey(Map.Entry<Object, Object> property) {
    return property.getKey().toString();
  }

  private String mapValue(Map.Entry<Object, Object> property) {
    return property.getValue().toString();
  }

  protected List<String> findKeysNames(Map<String, String> propertiesMap) {
    return propertiesMap.keySet()
        .stream()
        .filter(key -> key.startsWith(getFilterKeyPrefix()))
        .map(this::extractKeyName)
        .distinct()
        .sorted()
        .collect(Collectors.toList());
  }

  protected abstract String getFilterKeyPrefix();

  private String extractKeyName(String key) {
    return key.split("[.]")[2];
  }

  protected List<String> extractPatterns(String value) {
    return List.of(value.split("[,]"));
  }


  protected Map<String, String> findEntriesForKeyName(String foundedName, Map<String, String> propertiesMap) {
    return propertiesMap.entrySet()
        .stream()
        .filter(entry -> entry.getKey().startsWith(getFilterKeyPrefix() + foundedName + "."))
        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
  }
}
