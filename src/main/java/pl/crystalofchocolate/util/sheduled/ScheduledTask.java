package pl.crystalofchocolate.util.sheduled;


import java.util.concurrent.TimeUnit;

public interface ScheduledTask extends Runnable {

  Long getDelayTimeValue();

  TimeUnit getDelayTimeUnit();
}
