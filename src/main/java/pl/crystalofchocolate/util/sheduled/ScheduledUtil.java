package pl.crystalofchocolate.util.sheduled;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.concurrent.ScheduledExecutorService;

@RequiredArgsConstructor
public class ScheduledUtil implements AutoCloseable {
  @NonNull
  private final ScheduledExecutorService scheduledExecutorService;

  public void schedule(ScheduledTask scheduledTask) {
    scheduledExecutorService.scheduleWithFixedDelay(
        scheduledTask,
        scheduledTask.getDelayTimeValue(),
        scheduledTask.getDelayTimeValue(),
        scheduledTask.getDelayTimeUnit()
    );
  }

  @Override
  public void close() {
    scheduledExecutorService.shutdown();
  }

  public void submit(Runnable taskToRun) {
    scheduledExecutorService.submit(taskToRun);
  }
}
