package pl.crystalofchocolate.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.crystalofchocolate.picker.attachment.CollectedAttachment;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class NameUtil {
  public static final String FTP_FILTER_KEY_PREFIX = "filter.ftp.";
  public static final String FTP_SUBDIRECTORY = "ftp.subdirectory";
  public static final String FTP_DATE = "ftp.date";
  public static final String LOCAL_FTP_SUBDIRECTORY = "ftp-not-sent";
  public static final String PROPERTIES_SUFFIX = ".metadata";
  public static final String TELEGRAM_FILTER_KEY_PREFIX = "filter.telegram.";

  private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy_MM_dd-HH_mm_ss-");

  public static String prepareFileNameWithDate(CollectedAttachment collectedAttachment) {
    return collectedAttachment.getReceivedDate().format(formatter) + collectedAttachment.getName();
  }

  public static LocalDateTime stringToLocalDateTime(String textDate) {
    return LocalDateTime.parse(textDate, formatter);
  }

  public static String localDateTimeToString(LocalDateTime localDateTime) {
    return localDateTime.format(formatter);
  }

  public static LocalDateTime dateToLocalDateTime(Date date){
    return date.toInstant()
        .atZone(ZoneId.systemDefault())
        .toLocalDateTime();
  }

}
