package pl.crystalofchocolate.exceptions;

public class UnsupportedFileException extends RuntimeException {

  public UnsupportedFileException(String fileName) {
    super(String.format("Unsupported configuration extension %s", fileName));
  }
}
