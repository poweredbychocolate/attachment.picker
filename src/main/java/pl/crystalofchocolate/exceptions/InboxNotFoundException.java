package pl.crystalofchocolate.exceptions;

public class InboxNotFoundException extends RuntimeException {

  public InboxNotFoundException() {
    super("Inbox catalogue not found,check where mails are stored");
  }
}
