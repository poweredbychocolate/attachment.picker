package pl.crystalofchocolate.exceptions;

public class MissingConfigurationFileException extends RuntimeException {

  public MissingConfigurationFileException() {
    super("Arguments are empty, missing required config file path");
  }
}
