package pl.crystalofchocolate.exceptions;

public class IncorrectConfigurationFileException extends RuntimeException {

  public IncorrectConfigurationFileException() {
    super("Broken configuration file, unable to load configuration");
  }
}
