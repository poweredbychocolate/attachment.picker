package pl.crystalofchocolate.exceptions;

public class IncorrectMailBoxConfigurationException extends RuntimeException {

  public IncorrectMailBoxConfigurationException() {
    super("Incorrect mailbox configuration or authentication failed");
  }
}
