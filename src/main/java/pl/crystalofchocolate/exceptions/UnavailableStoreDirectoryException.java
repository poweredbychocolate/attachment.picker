package pl.crystalofchocolate.exceptions;

public class UnavailableStoreDirectoryException extends RuntimeException {

    public UnavailableStoreDirectoryException(String path) {
        super(String.format("Store directory cannot be accesses, incorrect path [%s] or permission", path));
    }
}
