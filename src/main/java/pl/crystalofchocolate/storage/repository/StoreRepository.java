package pl.crystalofchocolate.storage.repository;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import pl.crystalofchocolate.exceptions.UnavailableStoreDirectoryException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class StoreRepository {
  @NonNull
  private final StoreConnection storeConnection;
  private Path currentDirectory;

  public StoreRepository(@NonNull StoreConnection storeConnection) {
    this.storeConnection = storeConnection;
    try {
      Path rootPath = Paths.get(storeConnection.getPath());
      currentDirectory = prepareDirectory(rootPath);
    } catch (IOException e) {
      log.error("Incorrect root path,check it or edit permission", e);
      throw new UnavailableStoreDirectoryException(storeConnection.getPath());
    }
  }

  public void openDirectory(String directoryName) throws IOException {
    Path directoryPath = Paths.get(storeConnection.getPath(), directoryName);
    currentDirectory = prepareDirectory(directoryPath);
    log.debug(String.format("A current directory switched to %s", currentDirectory));
  }

  private Path prepareDirectory(Path directoryPath) throws IOException {
    return !Files.exists(directoryPath)
        ? Files.createDirectory(directoryPath)
        : directoryPath;
  }

  public void saveFile(String fileName, byte[] fileData) throws IOException {
    Path filePath = Files.createFile(preparePath(fileName));
    Files.write(filePath, fileData);
    log.debug(String.format("Saved file %s", fileName));
  }

  public boolean removeFile(String fileName) throws IOException {
    Path filePath = preparePath(fileName);
    return Files.deleteIfExists(filePath);
  }

  public boolean isFileExists(String fileName) {
    return Files.exists(preparePath(fileName));
  }

  private Path preparePath(String path) {
    return Paths.get(currentDirectory.toString(), path);
  }

  public List<String> filesList() throws IOException {
    try (Stream<Path> directoryList= Files.list(currentDirectory)){
      return directoryList.filter(filePath -> !Files.isDirectory(filePath))
          .map(Path::getFileName)
          .map(Path::toString)
          .collect(Collectors.toList());
    }
  }

  public byte[] fetchFileBytes(String fileName) throws IOException {
    Path filePath = preparePath(fileName);
    return Files.readAllBytes(filePath);
  }
}
