package pl.crystalofchocolate.storage.repository;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import pl.crystalofchocolate.config.AppProperties;

@Value
@Builder
public class StoreConnection {
  @NonNull
  String path;

  public static StoreConnection of(AppProperties config) {
    return StoreConnection.builder()
        .path(config.getStorePath())
        .build();
  }
}

