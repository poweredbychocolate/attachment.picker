package pl.crystalofchocolate.storage;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import pl.crystalofchocolate.picker.attachment.CollectedAttachment;
import pl.crystalofchocolate.storage.repository.StoreRepository;

import java.io.*;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;

import static pl.crystalofchocolate.util.NameUtil.*;

@RequiredArgsConstructor
public class StoreService {
  @NonNull
  private final StoreRepository storeRepository;

  public void storeAttachment(CollectedAttachment collectedAttachment, String directoryPath, Properties storeProperties) throws IOException {
    storeRepository.openDirectory(directoryPath);

    if (!storeRepository.isFileExists(collectedAttachment.getName())) {
      String originalFileName = collectedAttachment.getName();
      storeAttachment(originalFileName, collectedAttachment, storeProperties);
    } else {
      String fileNameWithDatePrefix = prepareFileNameWithDate(collectedAttachment);
      storeAttachment(fileNameWithDatePrefix, collectedAttachment, storeProperties);
    }
  }

  private void storeAttachment(String fileName, CollectedAttachment attachment, Properties properties) throws IOException {
    storeRepository.saveFile(fileName, attachment.getContent());
    Optional<byte[]> propertiesContext = preparePropertiesContext(properties);

    if (propertiesContext.isPresent()) {
      storeRepository.saveFile(fileName + PROPERTIES_SUFFIX, propertiesContext.get());
    }
  }

  private Optional<byte[]> preparePropertiesContext(Properties properties) throws IOException {
    if (Objects.nonNull(properties)) {
      ByteArrayOutputStream propertiesStream = new ByteArrayOutputStream();
      properties.store(propertiesStream, null);
      return Optional.of(propertiesStream.toByteArray());
    }
    return Optional.empty();
  }

  public boolean removeAttachment(String fileName, String directoryPath) throws IOException {
    storeRepository.openDirectory(directoryPath);
    storeRepository.removeFile(fileName + PROPERTIES_SUFFIX);
    return storeRepository.removeFile(fileName);
  }

  public List<String> getFilesList(String directoryPath) throws IOException {
    storeRepository.openDirectory(directoryPath);
    return storeRepository.filesList();
  }

  public byte[] fetchAttachmentContent(String directoryPath, String fileName) throws IOException {
    storeRepository.openDirectory(directoryPath);
    return storeRepository.fetchFileBytes(fileName);
  }

  public Properties fetchAttachmentProperties(String directoryPath, String fileName) throws IOException {
    storeRepository.openDirectory(directoryPath);
    byte[] bytes = storeRepository.fetchFileBytes(fileName + PROPERTIES_SUFFIX);
    Properties properties = new Properties();
    properties.load(new ByteArrayInputStream(bytes));
    return properties;
  }
}
