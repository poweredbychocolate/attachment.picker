# README
### Mail Attachment Picker

Mail Attachment Picker is a small tool that listens for incoming mail 
and extracts attachments to their destination. 

### Implemented attachment destinations:

- Telegram destination - filter attachment by defined filters 
  and if was catch send it to telegram bot api as message.
  
- FTP destination - allow filter attachment also, 
  matched attachment is uploaded to server directory
  or stored on local temporary directory,
  with additional metadata file if server is offline.
  Local stored attachments will be uploaded if when server will be online,
  it's done by additional thread that check connection periodically.

####
New destinations can be added by implementing CollectedAttachmentDestination interface

### Configuration file

Configuration file name need be passed as first program argument on run.
Only properties file is supported,
but other files types can be added by implementing ConfigLoader interface.
####
File example-config.properties contains example configuration template.
  
