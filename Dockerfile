FROM maven:3.6.3-openjdk-11 AS maven

RUN mkdir /app
RUN mkdir /app/sources

COPY src /app/sources/src
COPY pom.xml /app/sources

RUN mvn clean install -f /app/sources/pom.xml

FROM adoptopenjdk:11-jre-hotspot
ENV APP_CONFIG=config.properties
RUN mkdir /app
RUN mkdir /app/logs
VOLUME /app/logs
COPY --from=maven /app/sources/target/libraries  /app/libraries
COPY --from=maven /app/sources/target/attachment.picker-0.2-angel.jar  /app/ap.jar

COPY ${APP_CONFIG} /app
WORKDIR /app
CMD java -jar ap.jar ${APP_CONFIG}